#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Found the positions of a sublist in a list ##########

def is_Sublist(l, s):
	'''
		This function was found on 
		https://www.w3resource.com/python-exercises/list/python-data-type-list-exercise-32.php
		
		l : the list to search on
		
		s : the sublist
	'''
	
	st = 0
	ed = 0
	ll = []
	sub_set = False
	if s == []:
		sub_set = True
	elif s == l:
		sub_set = True
	elif len(s) > len(l):
		sub_set = False

	else:
		for i in range(len(l)):
			if l[i] == s[0]:
				n = 1
				while (n < len(s)) and (l[i+n] == s[n]):
					n += 1
				
				if n == len(s):
					sub_set = True
					st = i
					ed = i + n
					ll.append([st,ed])
					#return([st,ed])
	return(ll)
	

if __name__ == "__main__":
	is_Sublist()
