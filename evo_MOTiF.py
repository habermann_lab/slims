#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Pipeline to run a complete SLiM analysis for an input sequence ##########

#### Import modules ####
import click
import subprocess
import os
from Bio import SeqIO
from Bio import Phylo
import matplotlib.pyplot as plt
from tree_runner import tree_run
from get_path import get_script_path
import json
from pathlib import Path
from add_text import add_file_to_another
import pandas as pd
from add_taxonomy import add_taxonomy
from mmseqs_runner import mmseqs_runner
from orthodb_dl import orthodb_downloader

#### Setting variables ####

@click.command()
@click.argument("fasta_file")
@click.argument("results_path")
@click.option("-db","refseq_path",type=str,default=None, show_default=True, help='path to the refseq database')
@click.option("-o","orthology",type=str,default="blast", show_default=True, help='the orthology method to use (blast, orthodb or mmseqs2)')
@click.option("-p","proba_filter",type=float,default=None, show_default=True, help='a threshold to remove SLiMs with a too high value of occurence')
@click.option("-m","elm_motifs",type=str,default=None, show_default=True, help='a list of motifs\nBy default, the file elm_classes.tsv is used but you can personnalise your search by addind your own list.')
@click.option("-t","tree",type=str,default=None, show_default=True, help='whether to run or not PhyML or IQTree to make a tree with the orthologs found (phyml or iqtree')
@click.option("-c","cpus",type=int,default=8, show_default=True, help='number of CPUs to use for the orthologs search and for IQTree)')
@click.option("-s","taxid",type=int,default=2759, show_default=True, help='taxid to limit the BLAST search (2759 = eukaryota)')
@click.option("-mail","user_mail",type=str,default=None, show_default=True, help='enter your mail to allow the download of sequences (mandatory if you are using "-o mmseqs2")')
@click.option("-ro","redo_ortho",type=bool,default=True, show_default=True, help='Set "False" if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein')
@click.option("-eo","emergency_ortho",type=str,default=None, show_default=True, help='the orthology method to use (blast or mmseqs2) if orthodb did not worked')
@click.option("-cons","cons_thres",type=float,default=0.5, show_default=True, help='threshold for positional/aa property conservation score')
@click.option("-spe","spe_thres",type=float,default=0.5, show_default=True, help='threshold for overall conservation score')
@click.option("-dis","dis_thres",type=float,default=0.5, show_default=True, help='threshold for disorder score')
@click.option("-acc","acc_thres",type=float,default=6, show_default=True, help='threshold for surface accessibility score')
@click.option("-cdb","compdb",type=str,default=None, show_default=True, help='path to the refseq BLAST_formatted database')
@click.pass_context
def run_pipeline(ctx,fasta_file,results_path,refseq_path,orthology,proba_filter,elm_motifs,tree,cpus,taxid,user_mail,redo_ortho,emergency_ortho,cons_thres,spe_thres,dis_thres,acc_thres,compdb) :
	
	'''
	FASTA_FILE : file containing one sequence
	
		/!\ refseq format only
	
		example : >NP_001179.1 bcl-2 homologous antagonist/killer [Homo sapiens]
	
	RESULTS_PATH : path to the directory where you want to results to be
	
	This tool enables to study the conservation and the order of SLiMs
	found in an input sequence.
	
	The results can be found in the directories "RESULTS_PATH"/graph and "RESULTS_PATH"/table	
	'''
	fasta_file = os.path.abspath(fasta_file)
	results_path = os.path.abspath(results_path)
	if refseq_path != None :
		refseq_path = os.path.abspath(refseq_path)
	if compdb != None :
		compdb = os.path.abspath(compdb)
	if elm_motifs != None :
		elm_motifs = os.path.abspath(elm_motifs)
	
	#### Set the path to the directory ####
	try :
		path_pipeline = get_script_path()
		os.chdir(path_pipeline)
	except :
		pass
	
	#### Error message if the value entered is not good ####
	if proba_filter == None :
		pass
	elif proba_filter<0 or proba_filter>1 :
		print("A probability value is between 0 and 1.\nPlease, retry with a correct value !")
		return 0
	if orthology not in ["blast","orthodb","mmseqs2"] :
		print("Please enter a correct orthology method")
		return 0
	if orthology == "blast" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if tree not in ["iqtree","phyml",None,"None"] :
		print("Please, enter a correct value for the tree method")
		return 0
	if orthology == "mmseqs2" and refseq_path == None and user_mail == None :
		print("Please, don't forget to define a path to a refseq database and your mail while using MMseqs2")
		return 0
	if orthology == "mmseqs2" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if orthology == "mmseqs2" and user_mail == None :
		print("Please, don't forget to specify your mail while using MMseqs2")
		return 0
	if type(redo_ortho) != type(True) :
		print("-ro must be True or False, please re-set a correct value")
		return 0
	if emergency_ortho not in [None,"blast","mmseqs2"] :
		print("Please enter a correct secondary orthology method")
		return 0
	if orthology == "mmseqs2" and compdb == None :
		print("Please, don't forget to set the path to the BLAST-formatted database while using MMseqs2")
		return 0
	if elm_motifs == None :
		print("Please, don't forget to set the path to a motifs list")
		return 0

	
	print("\nStep 1 : We are looking for orthologs of your sequence\n")
	
	#### Creation of the RESULTS_PATH directory ####
	if not os.path.exists(results_path):
		os.makedirs(results_path)
	
	#### Extraction the identifier of the sequence of interest in order to set the names of futur files ####
	record = SeqIO.read(fasta_file, "fasta")
	input_fasta = str(record.name)
	#### Checking if a orthologs search was already done on this computer ####
	orthologs_file = str(record.name) + "_ortho.fa"
	ortho_file_test = None
	if redo_ortho == False :
		root_path = Path(fasta_file)
		print(root_path.parts[0])
		print(root_path.parts[1])
		print(root_path.parts[2])
		root_path = root_path.parts[0] + root_path.parts[1] + "/" + root_path.parts[2]
		for root, dirs, files in os.walk(root_path):
			if orthologs_file in files:
				ortho_file_test = root + "/" + orthologs_file
				break
	taxo_ortho = orthology # will be given to the taxonomy script
	#### Running the orthologs searcher script if the previous check failed ####
	if ortho_file_test == None :
		orthologs_file = results_path + "/" + str(record.name) + "_ortho.fa"
		if orthology == "blast" :
			ortholog_command = "python3 ortholog_searcher.py -i %s -o %s -d %s -f 95 -e 0.5 -c %s --max_blast_align=1000 --tax %s" % (fasta_file,results_path,refseq_path,cpus,taxid)
			subprocess.run(ortholog_command, shell=True)
		if orthology == "orthodb" :
			group_id = False
			orthodb_res = ctx.invoke(orthodb_downloader,fasta_file = fasta_file, output_dir = results_path)
			if orthodb_res == 0 :
				json_file = results_path + "/ortho_group.json"
				# Load the results as a dict
				with open(json_file) as f:
					data = json.load(f)
				# Extract the Id of the orthologous group
				group_id = data['data']
			# If no ortholog group was found, run blast or mmseqs2 instead
			if not group_id :
				if emergency_ortho == "blast" :
					print("No orthologs found on OrthoDB, will proceed with a BLAST search\n")
					ortholog_command = "python3 ortholog_searcher.py -i %s -o %s -d %s -f 95 -e 0.5 -c %s --max_blast_align=1000 --tax %s" % (fasta_file,results_path,refseq_path,cpus,taxid)
					subprocess.run(ortholog_command, shell=True)
					taxo_ortho = emergency_ortho # change the value given to the taxonomy script 
				elif emergency_ortho == "mmseqs2" :
					print("No orthologs found on OrthoDB, will proceed with a MMseqs2 search\n")
					ctx.invoke(mmseqs_runner,fasta_file = fasta_file, results_path = results_path, refseq_path = refseq_path,compdb = compdb, cpus = cpus, user_mail = user_mail)
					taxo_ortho = emergency_ortho # change the value given to the taxonomy script
				else :
					print("No orthologs found on OrthoDB, will continue without orthologs\n")
		if orthology == "mmseqs2" :
			ctx.invoke(mmseqs_runner,fasta_file = fasta_file, results_path = results_path, refseq_path = refseq_path,compdb = compdb, cpus = cpus, user_mail = user_mail)
	else :
		orthologs_file = ortho_file_test
		print("A list of orthologs for this protein already exists on your computer. We will use it !\n")
		print("\nStep 2 : Conversion to use SlimProb\n")
	
	print("\nStep 2 : Conversion to use SlimProb\n")
		
	#### Reformating the fasta files to make it readable for SlimProb ####
	orthologs_file_reformat = results_path +  "/" + str(record.name) + "_orthoreformat.fasta"
	seqsuite_command = "yes n | python2 SLiMSuite/tools/seqsuite.py prog=seqlist seqin=%s seqtype=prot rename=T seqout=%s" % (orthologs_file,orthologs_file_reformat)
	subprocess.run(seqsuite_command,shell=True)
	
	fasta_file_reformat = results_path +  "/" + str(record.name) + "reformat.fasta"
	seqsuite_command2 = "yes n | python2 SLiMSuite/tools/seqsuite.py prog=seqlist seqin=%s seqtype=prot rename=T seqout=%s" % (fasta_file,fasta_file_reformat)
	subprocess.run(seqsuite_command2,shell=True)
	
	record = SeqIO.read(fasta_file_reformat, "fasta")
	
	print("\nStep 3 : Multiple sequence alignment of the orthologs\n")
	
	#### Processing the multiple sequence alignment with mafft (default parameters) ####
	alignment_file = results_path +  "/" + str(record.name) + ".align"
	mafft_command = "yes | mafft %s > %s" % (orthologs_file_reformat, alignment_file)
	subprocess.run(mafft_command,shell=True)
		
	print("\nStep 4 : Looking for SLiMs in your sequence\n")
	
	#### Processing the search of SLiMs in the sequence of interest ####
	slimprob_output = results_path + "/slimprob_run"
	slimprob_command = "yes n | python3 run_slimprob.py %s %s 500 results.tsv %s %s" % (fasta_file_reformat,slimprob_output,elm_motifs,results_path)
	subprocess.run(slimprob_command, shell=True)
	
	print("\nStep 5 : Processing analysis of the results\n")
	
	#### Running statistics analysis on the results ####
	if proba_filter == 1 :
		proba_filter = None
	if proba_filter == None :
		r_command = "Rscript stats_calc.R -o %s -r %s -c %s -s %s -d %s -a %s" % (results_path,orthologs_file,cons_thres,spe_thres,dis_thres,acc_thres)
		subprocess.run(r_command, shell=True)
	else :
		r_command = "Rscript stats_calc.R -o %s -p %s -r %s -c %s -s %s -d %s -a %s" % (results_path,proba_filter,orthologs_file,cons_thres,spe_thres,dis_thres,acc_thres)
		subprocess.run(r_command, shell=True)
		
	if proba_filter != None :
		results_file = results_path + "/table/MOTiF_results_filtered.tsv" #Results from the pipeline
	else :
		results_file = results_path + "/table/MOTiF_results.tsv" #Results from the pipeline
	
	#if orthology != "orthodb" :
	add_taxonomy(results_file,alignment_file,results_path,input_fasta,elm_motifs,taxo_ortho) # Adding the taxonomy information to the results
		
	#### Run a tree construction if the option is set or if the number of orthologs is sufficient ####
	num = len([1 for line in open(orthologs_file) if line.startswith(">")])
	
	if tree == "phyml" or tree == "iqtree":
		if num > 2 :
			print("\nStep 6 : We are constructing the tree of your orthologs\n")
			record = SeqIO.read(fasta_file, "fasta")
			alignment_fasta = results_path +  "/" + str(record.name) + "align.fasta" #name of the alignement file
			mafft_command2 = "yes | mafft %s > %s" % (orthologs_file, alignment_fasta)
			subprocess.run(mafft_command2,shell=True)
			alignment_phylip = results_path +  "/" + str(record.name) + ".phy" #name of the converted alignment file
			tree_run(tree,alignment_fasta,alignment_phylip,cpus) #run the format converter and run PhyML/IQTree
			if tree == "phyml" :
				tree_file = alignment_phylip + "_phyml_tree.txt" #name of the tree file
			else :
				tree_file = alignment_phylip + ".treefile" #name of the tree file
			## Printing the file in a png file ##
			tree_read = Phylo.read(tree_file,'newick')
			tree_read.ladderize()
			png_file = results_path +  "/graph/tree.png"
			
			Phylo.draw(tree_read, do_show=False)
			plt.axis('off')
			plt.savefig(png_file,papertype = 'a4')
		else :
			print("\nCannot process a tree construction, too few orthologs\n")
	else :
		print("\nNo tree will be constructed.\n Wrong type or does not need.\n")
	
	print("\nYour job is done ! Thank you for using this tool\n")
	return 0
	
if __name__ == "__main__":
	run_pipeline()
