#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Extractor of sub-structure ##########

#### Import modules ####
import os
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.DSSP import DSSP
from sublist import is_Sublist
from struct_plot import visualize_secondary_structure
import subprocess


def pdb_parser(pdb_file,motif,png_file,new_pdb) :
	'''
		This module allows to extract a substructure from a PDB file pdb_file
		of a given motif
	'''
	
	#### Correspondance of the 3 letters to 1 letter amino acid code ####
	aa_conv = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
     'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N', 
     'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 
     'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}
	
	#### Read the PDB file ####
	parser = PDBParser(PERMISSIVE=1)
	structure_id = os.path.basename(pdb_file).rstrip(".pdb")
	print("Running on " + os.path.basename(pdb_file))
	print("Structure id : " + structure_id)
	structure = parser.get_structure(structure_id, pdb_file)
	name = structure.header["name"]
	print("Protein name : " + name)
	keywords = structure.header["keywords"]
	print("Keywords : " + keywords)
	
	try :
		#### Extract the modele from the PDB file ####
		model = structure[0]
		dssp = DSSP(model, pdb_file) # contain the informations about the modele (position, sequence, structure, ...)
		complete_sequence = "" # will contain the whole sequence in a string
		complete_seq_list = [] # will contain the whole sequence in a string
		motif_list = list(motif) # convert the motif string to a list
		position_list = [] # will contain all the position found in the PDB file /!\ necessary because some position are missing
		#### Fill the previous variables ####
		for j in list(dssp.keys()) :
			complete_seq_list.append(dssp[j][1])
			position_list.append(dssp[j][0])
			complete_sequence = complete_sequence + dssp[j][1]
		
		#### Check if the motif is found in the sequence ####
		if complete_sequence.find(motif) == -1 :
			print("Motif not found in this PDB file...")
			return("","","","")
		
		real_string_structure = "" # will contain a string corresponding to the structure
		real_sequence = "" # will contain the protein sequence associated with the structure
		
		#### Extract the position of the motif in the list ####
		sub = is_Sublist(complete_seq_list,motif_list)
		print(sub)
		if len(sub) == 1 :
			positions_in_list = sub[0]
		else :
			print("Motif found multiple times in this PDB file...")
			return("","","","")
		#### Position of the motif in the PDB file ####
		real_start = position_list[positions_in_list[0]]
		if positions_in_list[1] != len(position_list) :
			real_end = position_list[positions_in_list[1]]
		else :
			real_end = position_list.pop() + 1 # don't ask me why but it's necessary to have the last position of the list extracted
		
		print("From " + str(real_start) + " to " + str(real_end))
		
		for j in list(dssp.keys()) :
			if dssp[j][0] in [x for x in range(real_start,real_end)] : # for each position dssp[j][0], check if they are in the interval [start,end]
				res_struc = dssp[j][2] # extract the structure
				res_name = dssp[j][1] # extract the amino acid
				## Change the character corresponding to None to a letter
				if res_struc == "-" :
					real_string_structure = real_string_structure + "N"
				else : 
					real_string_structure = real_string_structure + res_struc
				real_sequence = real_sequence + res_name
		print(real_sequence + " = " + motif + " : " + str(real_sequence == motif))
		print("The secondary structure of the sequence " + real_sequence + " is " + real_string_structure)
		
		
		#### Return the structure if the motif sequence extracted correspond to the real motif ####
		if real_sequence == motif :
			visualize_secondary_structure(real_string_structure,png_file) # creation of a 2D representation of the motif
			
			#### Extraction of the 3D representation of the motif from the PDB file ####
			pdb_extraction_info = {}
			for model in structure :
				for chain in model :
					seq = [] # will contain the chain sequence in a list format
					seq_str = "" # will contain the chain sequence in a list format
					pos = [] # will contain the position of the AA (because some position are not AA)
					
					#### Extract each residue and its position ####
					for r in chain.get_residues() :
						if r.get_resname() in aa_conv :
							seq.append(aa_conv[r.get_resname()])
							seq_str = seq_str + aa_conv[r.get_resname()]
							pos.append(r.id[1])
					if seq_str.find(motif) != -1 :
						
						#### Extract the position of the motif in the list ####
						pos_find = is_Sublist(seq_str,motif_list)[0]
						
						#### Position of the motif in the PDB file ####
						st = pos[pos_find[0]]
						if pos_find[1] != len(pos) :
							en = pos[pos_find[1]]
						else :
							en = pos.pop() + 1 # don't ask me why but it's necessary to have the last position of the list extracted
						
						#### Store the information about the motif in a dict ####
						pdb_extraction_info["Chain"] = chain.id[0]
						pdb_extraction_info["Start"] = st
						pdb_extraction_info["End"] = en
						print(pdb_extraction_info)
						print("Use of PDB tools to extract the structure")
						tmp_file = os.path.abspath(new_pdb) + ".tmp" # file will contain the chain extracted
						
						#### Run PDB tools to extract the motif 3D structure ####
						select_chain_command = "python3 pdb-tools-2.0.0-rc1/pdbtools/pdb_selchain.py -%s %s > %s" % (pdb_extraction_info["Chain"],pdb_file,tmp_file)
						subprocess.run(select_chain_command, shell=True)
						select_motif_command = "python3 pdb-tools-2.0.0-rc1/pdbtools/pdb_selres.py -%s:%s %s > %s" % (pdb_extraction_info["Start"],pdb_extraction_info["End"],tmp_file,new_pdb)
						subprocess.run(select_motif_command, shell=True)
						
						#### Deletion of the temporary file ####
						os.remove(tmp_file)
						break
				if seq_str == "" :
					continue
				else :
					break
			return(real_string_structure,pdb_extraction_info["Start"],pdb_extraction_info["End"],pdb_extraction_info["Chain"])
		else :
			return("","","","")
	except :
		print("Can't process this PDB file...")
		return("","","","")
	
if __name__ == "__main__":
	pdb_parser()
