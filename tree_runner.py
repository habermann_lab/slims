#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Converter for fasta alignments and runner for PhyML/IQTree ##########

#### Import modules ####
import click
import subprocess
from Bio import SeqIO
from Bio import AlignIO
import os
import re

#### Setting variables ####

#@click.command()
#@click.argument("method")
#@click.argument("fasta_align")
#@click.argument("out_file")
#@click.argument("cpus")
def tree_run(method,fasta_align,out_file,cpus) :
	'''
	METHOD : method to use (phyml or iqtree)
	
	FASTA_ALIGN : alignment file in fasta format
	
	OUT_FILE : alignment file in phylip format
	
	CPUS : number of CPUs to use for IQTree
	
	This script convert fasta alignment file to phylip and run PhyML or IQTree
	'''
	tmp = "fasta_align.fa.tmp"
	fast_string = ""
	for record in SeqIO.parse(fasta_align, "fasta"):
		species = re.findall(r'\[([^]]*)\]', record.description)[0]
		ids = str(record.name)
		seq = str(record.seq)
		fast_string += ">" + ids + " " + species + "\n" + seq + "\n"
	f_tmp = open(tmp,"w")
	print(fast_string, file = f_tmp)
	f_tmp.close()
	
	#### Conversion of the alignment format from fasta to phylip ####
	AlignIO.convert(tmp, "fasta", out_file, "phylip-relaxed", alphabet=None)
	os.remove(tmp)
	
	if method == "phyml" :
		#### Run PhyML on protein sequence and 100 bootstrap ####
		phyml_command = "yes | phyml -i %s -d aa -m JTT -b 100" % (out_file)
		subprocess.run(phyml_command,shell=True)
	else :
		#### Run IQTree on protein sequence ####
		phyml_command = "yes | iqtree -s %s -T %s" % (out_file,cpus)
		subprocess.run(phyml_command,shell=True)
	

#if __name__ == "__main__":
#	tree_run()
