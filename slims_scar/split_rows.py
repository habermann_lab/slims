#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Module to split rows in multiple rows ##########

#### Import modules ####
import pandas as pd
import numpy as np

def explode(df, lst_cols, fill_value='', preserve_index=False):
	'''
		This function was found on 
		https://stackoverflow.com/questions/12680754/split-explode-pandas-dataframe-string-entry-to-separate-rows 
		and coded by MaxU.
		
		df : the pandas data frame on which apply the function
		
		lst_cols : list of columns which will be splited
		
	'''
    # make sure `lst_cols` is list-alike
	if (lst_cols is not None
		and len(lst_cols) > 0
		and not isinstance(lst_cols, (list, tuple, np.ndarray, pd.Series))):
		lst_cols = [lst_cols]
	# all columns except `lst_cols`
	idx_cols = df.columns.difference(lst_cols)
	# calculate lengths of lists
	lens = df[lst_cols[0]].str.len()
	# preserve original index values    
	idx = np.repeat(df.index.values, lens)
	# create "exploded" DF
	res = (pd.DataFrame({
				col:np.repeat(df[col].values, lens)
				for col in idx_cols},
				index=idx)
			.assign(**{col:np.concatenate(df.loc[lens>0, col].values)
					for col in lst_cols}))
	# append those rows that have empty lists
	if (lens == 0).any():
		# at least one list in cells is empty
		res = (res.append(df.loc[lens==0, idx_cols], sort=False)
					.fillna(fill_value))
	# revert the original index order
	res = res.sort_index()
	# reset index if requested
	if not preserve_index:        
		res = res.reset_index(drop=True)
	return res
	
if __name__ == "__main__":
	explode()
