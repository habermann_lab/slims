#!/usr/bin/python3
# -*- coding: utf-8 -*-

from Bio import SeqIO
import click
import subprocess
import json
import requests
from add_text import add_file_to_another

@click.command()
@click.argument("fasta_file")
@click.argument("output_dir")
def orthodb_downloader(fasta_file,output_dir):
	'''
	This script enable to find the orthologs of the sequence in 
	FASTA_FILE in OrthoDB
	
	FASTA_FILE : a single sequence fasta
	
	OUTPUT_DIR : the directory in which the results will be stored
	'''	
	# Load fasta file
	record = SeqIO.read(fasta_file, "fasta")
	# Extract the sequence
	sequence = record.seq
	# Create the url for OrthoDB API
	url_id = "https://www.orthodb.org/blast?seq=" + sequence
	# File for the first request output
	json_file = output_dir + "/ortho_group.json"
	# OrthoDB request to find the orthologous group of the sequence
	r = requests.get(url_id, allow_redirects=True)
	try:
		r.raise_for_status()
	except requests.exceptions.HTTPError:
		return "Failed"
	open(json_file, 'wb').write(r.content)
	# Load the results as a dict
	with open(json_file) as f:
		data = json.load(f)
	# Extract the Id of the orthologous group
	group_id = data['data']
	if not group_id :
		return(0)
	group_id = group_id[0]
	# Create the second url for OrthoDB API
	url_fasta = "https://www.orthodb.org/fasta?id=" + group_id
	# File for the second request output
	ortho_file = output_dir + "/" + str(record.name) + "_ortho.fa"
	# OrthoDB request to download the fasta sequences
	r2 = requests.get(url_fasta, allow_redirects=True)
	try:
		r2.raise_for_status()
	except requests.exceptions.HTTPError:
		return "Failed"
	open(ortho_file, 'wb').write(r2.content)
	# Concatenate the fasta files together
	add_file_to_another(fasta_file,ortho_file)
	print("Done")
	return 0

if __name__ == "__main__":
	orthodb_downloader()
