#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Fuse fasta files #########

def add_file_to_another(input_file,output_file) :
	outFile = open(output_file,"a+")
	with open(input_file) as inFile:
		for line in inFile:
			outFile.write(line)
	outFile.close()

if __name__ == "__main__":
	add_file_to_another()
