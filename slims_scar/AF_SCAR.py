#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Found structures of a list of motifs ##########

#### Import modules ####
import click
import subprocess
import os
from Bio.PDB import PDBList
import pandas as pd
from pdb_reader import pdb_parser
from split_rows import explode
from fasta2tab import fasta2tab
import csv
import warnings
from get_path import get_script_path
import urllib.parse
import urllib.request

#### Setting variables ####

@click.command()
@click.option("-i","motif_file",type=str, required=True, show_default=True, help='tsv file containing a list of proteins and their motifs')
@click.option("-a","alpha_path",type=str, required=True, show_default=True, help='path to an alpha fold database')
@click.option("-o","output_dir",type=str,default="~/ELM_structures", show_default=True, help='Output directory to store the results')
def pdb_elm_alt(motif_file,alpha_path,output_dir) :
	
	#### Set the path to the directory ####
	path_pipeline = get_script_path()
	os.chdir(path_pipeline)
	warnings.filterwarnings("ignore")
	
	#### Create directory ####
	directory_command = "mkdir -p %s" % (output_dir)
	subprocess.run(directory_command, shell=True)
	
	motifs = pd.read_csv(motif_file, sep='\t', header=0)
	motifs = motifs[motifs['UniProt'].notna()]
	
	motif_merged = motifs
	pdb_af_files = []
	for index, rows in motif_merged.iterrows():
		af_files = "AF-" + rows["UniProt"] + "-F1-model_v3.pdb"
		pdb_af_files.append(af_files)
	motif_merged["PDB"] = pdb_af_files
	
	motif_merged = motif_merged[motif_merged['UniProt'].notna()]
	motif_merged = motif_merged[motif_merged['PDB'].notna()]
	structure_list = [] # list of the structure which will be extracted from the PDB files
	pdbs_list = [] # list of all the PDBs model names
	pdbs_file_list = [] # list with the name of the files which will be downloaded
	structure_list = [] # list of the structure which will be extracted from the PDB files
	path_list = [] # list of the path to the PDB files
	motif_list = [] # list of the motifs 
	#### Fill the lists above ####
	for p in motif_merged["PDB"] :
		path_list.append(alpha_path + "/" + p)
	for m in motif_merged["Sequence"] :
		motif_list.append(m)

	#### For each motif, create a file in which the representation will be ####
	png_dir = output_dir + "/png" # directory with the alignments of the motifs
	directory_command = "mkdir -p %s" % (png_dir)
	subprocess.run(directory_command, shell=True)
	png_list = []
	c = 0
	for index, rows in motif_merged.iterrows():
		png = png_dir + "/" + rows["UniProt"] + "_" + rows["PDB"] + "_" + str(rows["Start_position"]) + "_" + str(rows["End_position"])
		png_list.append(png)
		c += 1
	motif_merged["PNG_file"] = png_list

	#### Same for a 3D representation of the motif ####
	pdb_shorten = []
	motifs_pdb_path = output_dir + "/" + "Motifs_PDB"
	directory_command = "mkdir -p %s" % (motifs_pdb_path)
	subprocess.run(directory_command, shell=True)
	for index, rows in motif_merged.iterrows():
		pdbs = motifs_pdb_path + "/" + rows["UniProt"] + "_" + rows["PDB"] + "_" + str(rows["Start_position"]) + "_" + str(rows["End_position"]) + ".pdb"
		pdb_shorten.append(pdbs)
	motif_merged["PDB_motif"] = pdb_shorten

	#### Looks for the structure for each motifs ####
	pdb_start_list = []
	pdb_end_list = []
	pdb_chain_list = []
	for i in range(len(png_list)) :
		if os.path.exists(path_list[i]) :
			l = pdb_parser(path_list[i],motif_list[i],png_list[i],pdb_shorten[i])
			#structure_list.append(pdb_parser(path_list[i],motif_list[i],png_list[i],pdb_shorten[i]))
			structure_list.append(l[0])
			pdb_start_list.append(l[1])
			pdb_end_list.append(l[2])
			pdb_chain_list.append(l[3])
		else :
			print("Structure does not exist, pass")
			structure_list.append("")
			pdb_start_list.append("")
			pdb_end_list.append("")
			pdb_chain_list.append("")
		print("\n")
	motif_merged["structure"] = structure_list # column corresponding to the structure pattern
	motif_merged["pdb_start"] = pdb_start_list # column corresponding to the structure pattern
	motif_merged["pdb_end"] = pdb_end_list # column corresponding to the structure pattern
	motif_merged["pdb_chain"] = pdb_chain_list # column corresponding to the structure pattern

	#### Delete rows for which no structure was found ####
	motif_merged = motif_merged[motif_merged['structure'] != ""]

	#### Remove the path from the PNG and PDB files to make it more readable ####
	png_shorten = []
	for files in motif_merged["PNG_file"] :
		png_shorten.append(os.path.basename(files) + ".png")
	motif_merged["PNG_file"] = png_shorten
	pdb_shorten = []
	for files in motif_merged["PDB_motif"] :
		pdb_shorten.append(os.path.basename(files))
	motif_merged["PDB_motif"] = pdb_shorten
	motif_merged = motif_merged[["UniProt","SLiM","Sequence","structure","Start_position","End_position","PDB","pdb_start","pdb_end","pdb_chain","PNG_file","PDB_motif"]]
	print(motif_merged.head())
	output_file = output_dir + "/STRUCTURE.tsv"
	motif_merged.to_csv(output_file,header = True, sep = "\t", quoting=csv.QUOTE_NONE, index=False)
	
if __name__ == "__main__":
	pdb_elm_alt()
