#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys

########## Function to export the script's path ##########
def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

if __name__ == "__main__":
	print(get_script_path())
