#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Add a taxonomy information to the results of the pipeline ##########

#### Import modules ####
from Bio import SeqIO
import re
import subprocess
import pandas as pd
import numpy as np
from ete3 import NCBITaxa

def add_taxonomy(results_file,alignment_file,results_path,input_fasta,elm_motifs,orthology) :
	'''
		RESULTS_FILE : Parsed results from SLiMProb
		
		ALIGNMENT_FILE : MAFFT alignment file
		
		RESULTS_PATH : path to the directory where you want to results to be
		
		INPUT_FASTA : Id of the input fasta file
		
		ELM_MOTIFS : File with the motifs of the protein
		
		ORTHOLOGY : Orthology method used
		
		This script parse the results and alignment from the pipeline to add information about 
		the taxonomy branch in which the motif is mostly found.
	'''
	
	tax_list = [] # List of the taxid in which each motif is found
	results_table = pd.read_csv(results_file, sep='\t', header=0) # Parsed slimprob results
	print(str(results_table.shape[0]) + " motifs\n" + "Input fasta : " + input_fasta)
	#### For each motif, looking for the taxonomy rank ####
	for index, row in results_table.iterrows():
		try :
			motif = str(row['Sequence']) # Exact motif
			motif_type = str(row['SLiM']) # Name of the motif
			# Start and End positions of the motif
			motif_start = int(row['Start_position'])
			motif_end = int(row['End_position'])
			# Open the motifs file to extract the regex
			with open(elm_motifs) as f:
				# Motifs file from ELM
				if '#ELM_Classes_Download_Version' in f.read():
					motifs_cut = results_path + "/" + "motifs_cut.tsv"
					motcut = open(motifs_cut,"w")
					with open(elm_motifs,"r") as motin :
						for line in motin:
							if not line.startswith('#'):
								motcut.write(line)
					motcut.close()
					motif_file = pd.read_csv(motifs_cut, sep='\t', header=0) # Read the motif file
					motif_file = motif_file[["ELMIdentifier", "Regex"]]
					motif_file = motif_file.rename(columns={'ELMIdentifier':'Type'}) # change the name of the column
					motif_file = motif_file.rename(columns={'Regex':'Seq'}) # change the name of the column
				# Generic motifs file
				else :
					motif_file = pd.read_csv(elm_motifs, sep='\t', header=None, names = ["Type","Seq"]) # Read the motif file
			#print(motif_file)
			motif_regex = motif_file.loc[motif_file['Type'] == motif_type].iloc[0]["Seq"] # Extract the motif regex
			motif_regex = re.compile(motif_regex) # Compile the regex
			len_motif = len(motif) # Length of the motif
			found_motif = "" # Motif in the alignment to be sure its the same
			species_list = [] # List containing the species in which the motif is found
			
			#### Looking for the position of the  motif in the input protein in the alignments ####
			for record in SeqIO.parse(alignment_file, "fasta") :
				if str(record.name) == "seq_UNK__" + input_fasta :
					try :
						species = re.findall(r'\[([^]]*)\]$', record.description)[0]
					except :
						species = record.description.split("[")[-1].rstrip("]")
						if "]" in species :
							species = "[" + species
					if "MULTISPECIES" not in str(record.description) :
						species_list.append(species)
					sequence = str(record.seq)
					count = 0 # Will be used to localize the position of the motif in the alignment
					start = 0 # Start position of the motif in the alignment
					end = 0 # End position of the motif in the alignment
					st_already_set = False
					ed_already_set = False
					for i in range(len(sequence)) :
						if sequence[i] != "-" :
							count += 1 # Does not count the position with gaps
						if count == motif_start and (not st_already_set) :
							start = i
							st_already_set = True
						if count == motif_end  and (not ed_already_set) :
							end = i+1
							ed_already_set = True
					found_motif = str(record.seq)[start:end].replace('-', '')
					
					print("\nRecord name : " + str(record.name) + "\nSpecies : " + species + "\nInput motif type : " + motif_type + "\nInput motif : " + motif + "\nMotif found : " + found_motif + " " + str(bool(re.match(motif_regex,str(record.seq)[start:end].replace('-', '')))) + "\nStart : " + str(start) + "\nEnd : " + str(end) + "\nMotif length : " + str(len_motif) + "\nCount : " + str(count) + "\nSequence length : " + str(len(sequence)) + "\nMotif from start to end : " + sequence[start:end])
			
			#### Looking for the motif in the alignment at the given position ####
			for record in SeqIO.parse(alignment_file, "fasta") :
				if str(record.name) != "seq_UNK__" + input_fasta :
					if "MULTISPECIES" in str(record.description) :
						continue
					# OrthoDB orthologs are not formated the same way, thus they need a different parsing
					if orthology != "orthodb" :
						if not re.findall(r'\[([^]]*)\]$', record.description) :
							continue
						species = re.findall(r'\[([^]]*)\]$', record.description)[0].replace('_', ' ') # retrive the species name from the sequence ID
						matching = bool(re.match(motif_regex,str(record.seq)[start:end].replace('-', ''))) # Check if the motif is present at the position
						if matching :
							# add the species to the list if the motif is found
							if species not in species_list :
								species_list.append(species) 
					else :
						txid = int(record.id.split("_")[3]) # taxids are stored on the sequence IDs
						matching = bool(re.match(motif_regex,str(record.seq)[start:end].replace('-', ''))) # Check if the motif is present at the position
						if matching :
							# convert the taxid to its corresponding species to add it to the list
							ncbi = NCBITaxa()
							taxid2name = ncbi.get_taxid_translator([txid])
							species = taxid2name[txid]
							if species not in species_list :
								species_list.append(species)
							
			#### Looking for the taxids associated with each species containing the motif ####
			taxa_file_content = '\n'.join(species_list)
			taxa_file = results_path + "/species_list.txt" # Store the species in a file
			speciesFile = open(taxa_file, "w")
			print(taxa_file_content,file=speciesFile)
			speciesFile.close()
			taxa_ids_file = results_path + "/taxids.tsv" # File which will contain the taxids
			taxa_command = "taxaranks -i %s -o %s -t" % (taxa_file,taxa_ids_file) # Using the tool for taxid retrieving
			subprocess.run(taxa_command, shell=True)
			
			#### Parse the taxids to find the common one to each species ####
			taxids_table = pd.read_csv(taxa_ids_file, sep='\t', header=0)
			common_taxname = None
			
			if taxids_table.shape[0] == 1 :
				used_column = taxids_table.iloc[: , -1]
				column_name = taxids_table.iloc[: , -1].name
				common_taxid = used_column[0]
				print("Taxid : " + str(common_taxid))
				common_taxname = taxids_table[column_name.split("_")[0]][0]
			
				print("Taxname : " + common_taxname)
				tax_list.append(str(common_taxname) + " (" + str(common_taxid) + ")")
			elif taxids_table.shape[0] == 0 :
				tax_list.append(np.nan)
			else :
				for i in range(len(taxids_table.columns)) :
					s = taxids_table.iloc[: , -i] # Parse from last to first column
					a = s.to_numpy()
					if (a[0] == a).all() :
						# if all taxid in a column are common, it is kept
						used_column = taxids_table.iloc[: , -i]
						column_name = taxids_table.iloc[: , -i].name
						common_taxid = used_column[0]
						print("Taxid : " + str(common_taxid))
						common_taxname = taxids_table[column_name.split("_")[0]][0]
			
						print("Taxname : " + common_taxname)
						tax_list.append(str(common_taxname) + " (" + str(common_taxid) + ")")
						break
				# if no common taxid is found between the motifs homologs, a default description is given
				# especially, it was coded for the cases of viruses proteins
				if common_taxname == None :
					tax_list.append("Living organisms (NoComTax)")
		except :
			tax_list.append(np.nan)
		
	#### Add the results to the result table ####
	results_table["Taxonomy"] = tax_list
	results_table.to_csv(results_file, sep = '\t', index=False, header = True) # print the updated table to file
			
if __name__ == "__main__" :
	add_taxonomy()
