#!/usr/bin/python3
# -*- coding: utf-8 -*-

import urllib.parse
import urllib.request
import subprocess
from Bio import Entrez
from Bio import SeqIO
import click
from get_path import get_script_path
import os
import pandas as pd
import time
from evo_MOTiF import run_pipeline
import mygene

@click.command()
@click.argument("input_file")
@click.argument("results_path")
@click.option("-db","refseq_path",type=str,default=None, show_default=True, help='path to the refseq database')
@click.option("-dt","data_type",type=str,default=None, show_default=True, help='type of the dataset (elm, psp or xlms)')
@click.option("-m","motifs",type=str,default=None, show_default=True, help='list of SLiMs from elm.eu.org (for ELM datasets only)')
@click.option("-o","orthology",type=str,default="blast", show_default=True, help='the orthology method to use (blast, orthodb or mmseqs2)')
@click.option("-t","tree",type=str,default=None, show_default=True, help='whether to run or not PhyML or IQTree to make a tree with the orthologs found (phyml or iqtree')
@click.option("-c","cpus",type=int,default=8, show_default=True, help='number of CPUs to use for the orthologs search and for IQTree)')
@click.option("-s","taxid",type=int,default=2759, show_default=True, help='taxid to limit the BLAST search (2759 = eukaryota)')
@click.option("-mail","user_mail",type=str,default=None, show_default=True, help='enter your mail to allow the download of sequences')
@click.option("-ro","redo_ortho",type=bool,default=True, show_default=True, help='Set "False" if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein')
@click.option("-eo","emergency_ortho",type=str,default=None, show_default=True, help='the orthology method to use (blast or mmseqs2) if orthodb did not worked')
@click.option("-cons","cons_thres",type=float,default=0.5, show_default=True, help='threshold for positional conservation score')
@click.option("-spe","spe_thres",type=float,default=0.66, show_default=True, help='threshold for overall conservation score')
@click.option("-dis","dis_thres",type=float,default=0.5, show_default=True, help='threshold for disorder score')
@click.option("-acc","acc_thres",type=float,default=6, show_default=True, help='threshold for surface accessibility score')
@click.option("-cdb","compdb",type=str,default=None, show_default=True, help='path to the refseq BLAST_formatted database')
@click.pass_context
def run_pipeline_on_dataset(ctx,input_file,results_path,refseq_path,data_type,motifs,orthology,tree,cpus,taxid,user_mail,redo_ortho,emergency_ortho,cons_thres,spe_thres,dis_thres,acc_thres,compdb) :
	'''
	This script allows to run the pipeline on ELM (E), crosslinking (X) 
	or PhosphoSitePlus (P) data, after their conversion to make them readable
	
	INPUT_FILE : the dataset to study
	
	RESULTS_PATH : path to the directory where you want to results to be
	'''
	input_file = os.path.abspath(input_file)
	results_path = os.path.abspath(results_path)
	if refseq_path != None :
		refseq_path = os.path.abspath(refseq_path)
	if motifs != None :
		motifs = os.path.abspath(motifs)
	if compdb != None :
		compdb = os.path.abspath(compdb)
	
	begining_time = int(time.time())
	#### Set the path to the directory ####
	path_pipeline = get_script_path()
	os.chdir(path_pipeline)
	
	#### Error message if the value entered is not good ####
	if orthology not in ["blast","orthodb","mmseqs2"] :
		print("Please enter a correct orthology method")
		return 0
	if data_type not in ["elm", "psp", "xlms"] :
		print("Please enter a valid dataset type")
	if data_type == "elm" and motifs == None :
		print("Don't forget to set a motif file from elm")
	if orthology == "blast" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if tree not in ["iqtree","phyml",None,"None"] :
		print("Please, enter a correct value for the tree method")
		return 0
	if orthology == "mmseqs2" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if user_mail == None :
		print("Please, don't forget to specify your mail")
		return 0
	if type(redo_ortho) != type(True) :
		print("-ro must be True or False, please re-set a correct value")
		return 0
	if emergency_ortho not in [None,"blast","mmseqs2"] :
		print("Please enter a correct secondary orthology method")
		return 0
	if orthology == "mmseqs2" and compdb == None :
		print("Please, don't forget to set the path to the BLAST-formatted database while using MMseqs2")
		return 0
	
	#### Create directory ####
	if not os.path.exists(results_path):
		os.makedirs(results_path)
	
	#### Set mail to Entrez download ####
	Entrez.email = user_mail
	
	#### Removing the comment lines at the beginning of the input files from elm ####
	if data_type == "elm" :
		input_cut = results_path + "/" + "input_cut.tsv"
		
		motifs_cut = results_path + "/" + "motifs_cut.tsv"
		
		incut = open(input_cut,"w")
		with open(input_file,"r") as inin :
			for line in inin:
				if not line.startswith('#'):
					incut.write(line)
		incut.close()
		
		motcut = open(motifs_cut,"w")
		with open(motifs,"r") as motin :
			for line in motin:
				if not line.startswith('#'):
					motcut.write(line)
		motcut.close()

	
	#### Conversion from Uniprot Ids to RefSeq Ids, adaptated to each type of dataset ####
	if data_type == "xlms" :
		uniprot_motifs = pd.read_csv(input_file, sep='\t', header=0, names = ["ID","Motifs"]) # xlms instances file
		uniprot_motifs = uniprot_motifs.drop_duplicates(subset=['ID','Motifs'], keep="first") # remove duplicated motifs
	elif data_type == "psp" :
		psp_instances = pd.read_csv(input_file, sep='\t', header=0) # psp instances file
		uniprot_motifs = psp_instances[['ACC_ID','SITE_+/-7_AA']] # select only motifs and their uniprot ids
		uniprot_motifs = uniprot_motifs.rename(columns={'ACC_ID':'ID'}) # rename the columns
		uniprot_motifs = uniprot_motifs.rename(columns={'SITE_+/-7_AA':'Motifs'})
		uniprot_motifs = uniprot_motifs.drop_duplicates(subset=['ID','Motifs'], keep="first") # remove duplicated motifs
	else :
		elm_instances = pd.read_csv(input_cut, sep='\t', header=0) # elm instances file
		motifs_instances = pd.read_csv(motifs_cut, sep='\t', header=0) # elm classes file
		elm_merged = pd.merge(elm_instances, motifs_instances, on="ELMIdentifier",how="left") # merge them to extract the motif pattern
		uniprot_motifs = elm_merged[["Primary_Acc","Regex","ELMIdentifier"]] # select only motifs and their uniprot ids
		uniprot_motifs = uniprot_motifs.rename(columns={'Primary_Acc':'ID'}) # rename the columns
		uniprot_motifs = uniprot_motifs.rename(columns={'Regex':'Motifs'})
		uniprot_motifs = uniprot_motifs.drop_duplicates(subset=['ID','Motifs',"ELMIdentifier"], keep="first") # remove duplicated motifs
	uniprot_motifs['ID'] = uniprot_motifs['ID'].str.upper()
	if data_type != "elm" :
		uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.replace('o','.')
		uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.upper()
		if data_type == "xlms" :
			uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.replace('_','')
		else :
			uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.replace(r'^_+','^')
			uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.replace(r'_+$','$')
			uniprot_motifs['Motifs'] = uniprot_motifs['Motifs'].str.replace('_','')
	
	uniprot_ids = [] # will contain the uniprot identifiers
	## Fill the list above with unique accessions
	for ids in uniprot_motifs['ID']:
		if ids not in uniprot_ids :
			uniprot_ids.append(str(ids))
			
	query = ' '.join(uniprot_ids) # transform the list in query string for the conversion
	print("Converting to RefSeq ids")
	starttime = int(time.time())
	
	# ~ url = 'https://www.uniprot.org/uploadlists/'
	
	# ~ params = {
	# ~ 'from': 'ACC+ID',
	# ~ 'to': 'P_REFSEQ_AC',
	# ~ 'format': 'tab',
	# ~ 'query': query
	# ~ }
	
	# ~ data = urllib.parse.urlencode(params)
	# ~ data = data.encode('utf-8')
	# ~ req = urllib.request.Request(url, data)
	# ~ with urllib.request.urlopen(req) as f:
		# ~ response = f.read()
	path_ids = results_path + "/" + "id.tsv" # file containing the corresponding uniprot/refseq IDs
	# ~ out = open(path_ids,"w")
	# ~ print(response.decode('utf-8'), file=out)
	# ~ out.close()
	query_list = query.split(" ")
	uniprot_refseq = pd.DataFrame(columns = ['From', 'To'])
	mg = mygene.MyGeneInfo()
	json_mg = mg.querymany(query_list, fields = "refseq.protein",scopes = "uniprot")
	for prot in json_mg :
		q = prot["query"]
		if "refseq" in prot :
			prot_list = prot["refseq"]["protein"]
			#print(prot_list)
			if isinstance(prot_list, list):
				for refel in prot_list :
					uniprot_refseq = uniprot_refseq.append({'From' : q, 'To' : refel}, ignore_index = True)
			else :
				uniprot_refseq = uniprot_refseq.append({'From' : q, 'To' : prot_list}, ignore_index = True)
	uniprot_refseq.to_csv(path_ids, sep = '\t', index=False, header = True) # print the new table to file
	#### Create a directory containing files with each motifs of each sequences ####
	out_motifs = results_path + "/motifs"
	if not os.path.exists(out_motifs):
		os.makedirs(out_motifs)
	
	#uniprot_refseq = pd.read_csv(path_ids, sep='\t', header=0) # file with the corresponding UniProt RefSeq ids
	#####
	tot_refseq = results_path + "/" + "tot_refseq.fasta" # file containing all the downloaded sequences
	ref_ids = uniprot_refseq['To']
	unip_ids = list(set(uniprot_refseq['From']))
	query = ','.join(ref_ids)
	with open(tot_refseq,"w") as outFile :
		handle = Entrez.efetch(db = 'protein', id = query, rettype = 'fasta', retmod = 'text')
		for lines in handle :
			print(lines.strip(),file=outFile)
		handle.close()
		
	refseq_seqlen = pd.DataFrame({'To' : [],"Length.To" : []})
	for records in SeqIO.parse(tot_refseq, "fasta"):
		dict_seq = {'To' : records.id,"Length.To" : str(len(records.seq))}
		refseq_seqlen = refseq_seqlen.append(dict_seq, ignore_index = True)
	#print(refseq_seqlen)
	
	
	tot_uniprot = results_path + "/" + "tot_uniprot.fasta"
	query = ','.join(unip_ids)
	uniprot_seqlen = pd.DataFrame({'From' : [],"Length.From" : []})
	with open(tot_uniprot,"w") as outFile :
		handle = Entrez.efetch(db = 'protein', id = query, rettype = 'fasta', retmod = 'text')
		for lines in handle :
			print(lines.strip(),file=outFile)
		handle.close()
	for records in SeqIO.parse(tot_uniprot, "fasta"):
		dict_seq = {'From' : records.id.split("|")[1].split(".")[0],"Length.From" : str(len(records.seq))}
		uniprot_seqlen = uniprot_seqlen.append(dict_seq, ignore_index = True)
	#print(uniprot_seqlen)
	
	uniprot_refseq = pd.merge(uniprot_refseq,refseq_seqlen,left_on = "To", right_on="To", how = "left")
	uniprot_refseq = pd.merge(uniprot_refseq,uniprot_seqlen,left_on = "From", right_on="From", how = "left")
	uniprot_refseq = uniprot_refseq.loc[uniprot_refseq['Length.From'] == uniprot_refseq['Length.To']]
	uniprot_refseq = uniprot_refseq[["From", "To"]]
	#####
	unique_uniprot_refseq = uniprot_refseq.drop_duplicates(subset='From', keep="first") # remove duplicated ids
	id_merged = pd.merge(unique_uniprot_refseq, uniprot_motifs, left_on='From', right_on='ID') # merge the motifs with the refseq ids
	id_merged = id_merged.drop_duplicates() # remove duplicates
	id_merged_unique = id_merged.drop_duplicates(subset='To', keep="first")
	refseq_ids = [] # will contain all the refseq ids
	
	## Fill the list above with unique accessions
	for ids in id_merged_unique['To']:
		if ids not in refseq_ids :
			refseq_ids.append(ids)
	motifs_list = []
	count = 0 
	for refseq_id in id_merged["To"] :
		if data_type == "xlms" :
			motifs_list.append("XLMS_" + str(count) + refseq_id)
			count += 1
		elif data_type == "psp" :
			motifs_list.append("PSP_" + os.path.basename(input_file) + "_" + str(count) + refseq_id)
			count += 1
	if data_type != "elm" :
		id_merged["Motifs_name"] = motifs_list
	
	## For each Id, create a file containing its motifs
	for refseq in refseq_ids :
		newdf = id_merged[id_merged["To"] == refseq]
		file_motif = out_motifs + "/" + refseq
		if data_type != "elm" :
			newdf = newdf[["Motifs_name","Motifs"]]
		else :
			newdf = newdf[["ELMIdentifier","Motifs"]]
		newdf.to_csv(file_motif, sep = '\t', index=False, header = False)
	print('Done : {} sec\n'.format(int(time.time() - starttime)))
	
	#### Download sequences from Batch Entrez ####
	print("Downloading the sequences")
	starttime = int(time.time())
	query = ','.join(refseq_ids)	
	
	path_refseq = results_path + "/" + "refseq.fasta" # file containing all the downloaded sequences
	with open(path_refseq,"w") as outFile :
		handle = Entrez.efetch(db = 'protein', id = query, rettype = 'fasta', retmod = 'text')
		for lines in handle :
			print(lines.strip(),file=outFile)
		handle.close()
	print('Done : {} sec\n'.format(int(time.time() - starttime)))
	
	#### Convert the fasta file to tab ####
	path_refseq_converted = results_path + "/" + "refseq_converted.tab" # a tab version of the fasta file
	with open(path_refseq_converted,"w") as outFile :
		for record in SeqIO.parse(path_refseq, "fasta"):
			print(record.id + "\t" + record.description + "\t" + record.seq, file = outFile)
	
	#### Remove possible sequence downloaded multiple times ####
	path_refseq_converted_min = results_path + "/" + "refseq_converted_short.tab"
	lines_seen = set()
	with open(path_refseq_converted_min, "w") as output_file:
		for each_line in open(path_refseq_converted, "r"):
			if each_line not in lines_seen :
				output_file.write(each_line)
				lines_seen.add(each_line)
	
	path_fastas = results_path + "/" + "fastas"
	if not os.path.exists(path_fastas):
		os.makedirs(path_fastas)
	count_sequences = 0
	filesList = list()
	#### Reading the input file to put one sequence per file ####
	with open(path_refseq,"r") as inFile :
		text = ""
		fileCount = 0
		
		#### For the sequences in the input, it creates files with 1 sequence ####
		for line in inFile :
			if line.startswith('>') :
				if count_sequences > 0 :
					name = str(path_fastas) + "/" + str(count_sequences) + ".fasta"
					filesList.append(name)
					with open(name,"w") as outfile :
						outfile.write(text)
					text = ""
				count_sequences += 1
			text += line
		name = str(path_fastas) + "/" + str(count_sequences) + ".fasta"
		filesList.append(name)
		with open(name,"w") as outfile :
			outfile.write(text)

	counter = 0
	#### For each fasta file, run the pipeline with the given motifs ####
	fastas_path = results_path + "/fastas"
	for file in os.listdir(fastas_path):
		if file.endswith(".fasta"):
			## Read the fasta file
			fasta_file = results_path + "/fastas/" + file
			record = SeqIO.read(fasta_file, "fasta")
			## Creation of the output directory
			output_path = results_path + "/" + str(record.name)
			motifs_file = results_path + "/motifs/" + str(record.name)
			## Run the pipeline
			ctx.invoke(run_pipeline, fasta_file = fasta_file, results_path = output_path,orthology = orthology, elm_motifs = motifs_file, tree = tree, cpus = cpus, taxid = taxid,refseq_path = refseq_path, user_mail = user_mail, redo_ortho = redo_ortho, emergency_ortho = emergency_ortho,proba_filter = None,cons_thres = cons_thres,spe_thres = spe_thres,dis_thres = dis_thres,acc_thres = acc_thres,compdb = compdb)
			## Run the script which gave back the UniProt IDs
			slimprob_res = output_path + "/table/MOTiF_results.tsv" # path to the results of the pipeline
			if os.path.exists(slimprob_res) :
				slimprob_results = pd.read_csv(slimprob_res, sep='\t', header=0) # results of the pipeline
				res_and_ids = pd.merge(slimprob_results, unique_uniprot_refseq, left_on='Accession', right_on='To',how="left") # add the UniProt ids to the results file
				res_and_ids = res_and_ids[["Accession","From","SLiM","Start_position","End_position","Sequence","Positional_conservation_score","Positional_conservation","Disorder_score","Order","Surface_accessibility_score","Surface_accessibility",'Protein_homologs',"Overall_conservation_score","Overall_conservation","AA_property_conservation_score","AA_property_conservation","Probability","Taxonomy"]]
				res_and_ids = res_and_ids.rename(columns={'From':'UniProt'}) # change the name of the column
				if data_type != "elm" :
					output_table_tp = res_and_ids[res_and_ids['Positional_conservation'].notna()]
					output_table_tp.to_csv(slimprob_res, sep = '\t', index=False, header = True) # print the new table to file
				else :
					slimprob_res_tp = output_path + "/table/MOTiF_results_tp.tsv" # file which will contain only true positives from the results
					output_table_tp = pd.merge(res_and_ids,elm_instances,left_on = ["UniProt","SLiM","Start_position","End_position"], right_on=["Primary_Acc","ELMIdentifier","Start","End"], how = "right") # select only true positives
					output_table_tp = output_table_tp[["Accession_x","UniProt","SLiM","Start_position","End_position","Sequence","Positional_conservation_score","Positional_conservation","Disorder_score","Order","Surface_accessibility_score","Surface_accessibility",'Protein_homologs',"Overall_conservation_score","Overall_conservation","AA_property_conservation_score","AA_property_conservation","Probability","Taxonomy"]]
					output_table_tp = output_table_tp.rename(columns={'Accession_x':'Accession'})
					output_table_tp = output_table_tp[output_table_tp['Positional_conservation'].notna()]
					# Now we will convert some columns into 'int64' type.
					output_table_tp.Start_position = output_table_tp.Start_position.astype('int64')
					output_table_tp.End_position = output_table_tp.End_position.astype('int64')
					#output_table_tp.Motif_homologs = output_table_tp.Motif_homologs.astype('int64')
					output_table_tp.Protein_homologs = output_table_tp.Protein_homologs.astype('int64')
					output_table_tp.to_csv(slimprob_res_tp, sep = '\t', index=False, header = True) # print the new table to file
			if counter == 0 :
				header = output_path
			counter += 1
	
	#### Concatenation of all the results in one tsv file
	if data_type == "xlms" :
		output_file = results_path + "/XLMS_results.tsv"
	elif data_type == "psp" :
		output_file = results_path + "/PSP_results.tsv"
	else :
		output_file = results_path + "/ELM_results.tsv"
	all_path = results_path + "/*"
	
	## Retrieve the header of the file in the first result
	if data_type != "elm" :
		header_command = "head -1 %s/table/MOTiF_results.tsv > %s" % (header,output_file)
	else :
		header_command = "head -1 %s/table/MOTiF_results_tp.tsv > %s" % (header,output_file)
	subprocess.run(header_command,shell=True)
	## Concatenate all the results in this file (header not counted)
	if data_type != "elm" :
		concatenate_command = "tail -n +2 -q %s/table/MOTiF_results.tsv >> %s" % (all_path,output_file)
	else :
		concatenate_command = "tail -n +2 -q %s/table/MOTiF_results_tp.tsv >> %s" % (all_path,output_file)
	subprocess.run(concatenate_command,shell=True)
	
	print('Time spent : {} \n'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - begining_time))))

if __name__ == "__main__":
	run_pipeline_on_dataset()
