#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Runner for the MMseqs2 search mode ##########

#### Import modules ####
import click
import subprocess
from Bio import Entrez
from Bio import SeqIO
from add_text import add_file_to_another
import os
import os.path
import re
import pandas as pd
import json
import mygene

#### Setting variables ####

@click.command()
@click.argument("fasta_file")
@click.argument("results_path")
@click.option("-db","refseq_path",type=str,default=None, show_default=True, help='path to the refseq database')
@click.option("-cdb","compdb",type=str,default=None, show_default=True, help='path to the refseq BLAST_formatted database')
@click.option("-c","cpus",type=int,default=8, show_default=True, help='number of CPUs to use for the orthologs search and for IQTree)')
@click.option("-mail","user_mail",type=str,default=None, show_default=True, help='enter your mail to allow the download of sequences')
def mmseqs_runner(fasta_file,results_path,refseq_path,compdb,cpus,user_mail):
	'''
		FASTA_FILE : file containing one sequence
	
		RESULTS_PATH : path to the directory where you want to results to be
		
		This module aims at running MMseqs2 with the input file, by 
		first converting it to a compatible file format.
	'''
	
	# Email to use entrez
	Entrez.email = user_mail
	# Read the fasta file
	record = SeqIO.read(fasta_file, "fasta")
	fasta_name = record.name # Same but not a regex
	print("Running on " + str(fasta_name) + "\n")
	
	# Prepare the path to the file to use MMseqs2
	queryDB = results_path + "/queryDB" # The fasta file converted to a MMseqs2 file
	searchDB = results_path + "/searchDB" # Result file of MMseqs2
	searchDB_converted = results_path + "/searchDB.m8" # Result file converted to an easy readable file
	tmp_dir = results_path + "/tmp" # Directory for temporary files
	if os.path.exists(searchDB_converted) == False :
		# Conversion of the fasta file 
		createDB_command = "mmseqs createdb %s %s" % (fasta_file,queryDB)
		subprocess.run(createDB_command, shell=True)
		# Run the MMseqs search
		search_command = "mmseqs search %s %s %s %s -s 7 --threads %s" % (queryDB,refseq_path,searchDB,tmp_dir,cpus)
		subprocess.run(search_command, shell=True)
		# Conversion to an easy readable file
		convert_command = "mmseqs convertalis %s %s %s %s --threads %s" % (queryDB,refseq_path,searchDB,searchDB_converted,cpus)
		subprocess.run(convert_command, shell=True)
	
	# List which will contain all the orthologs identifiers
	ids = []
	
	# Read the MMseqs results and extraction of the RefSeq identifiers
	with open(searchDB_converted) as infile:
		for line in infile:
			ids.append(line.split('\t')[1])
	# Make sure there is no duplicated identifiers
	ids_unique = list(set(ids))
	# Convert the list to a string
	query = ','.join(ids_unique)
	
	ortho_file = results_path + "/" + str(record.name) + "_ortho.fa"
	
	# Download of the sequences and adding them to a fasta file
	with open(ortho_file,"w") as outFile :
		handle = Entrez.efetch(db = 'protein', id = query, rettype = 'fasta', retmod = 'text')
		for lines in handle :
			print(lines.strip(),file=outFile)
		handle.close()
	
	# Parsing the results
	searchDB_pandas = pd.read_csv(searchDB_converted, sep='\t', header=None) # Results of the first MMseqs2 search
	
	pattern = re.compile(record.name) # Name of the first protein
	
	fasta_description = record.description # Id + name + description of the protein
	fasta_seq = record.seq # sequence of the input
	fasta_name_len = len(fasta_name) # Length of the name of the protein
	fasta_length = len(record.seq) # length of the protein
	mgq = "refseq:" + str(record.id)
	mg = mygene.MyGeneInfo()
	json_mg = mg.query(q = mgq, fields = "entrezgene")
	try :
		geneid = int(json_mg['hits'][0]['entrezgene'])
	except :
		geneid = None
	try :
		fasta_spe = re.findall(r'\[([^]]*)\]$', record.description)[0] # Species of the protein
	except :
		fasta_spe = record.description.split("[")[-1].rstrip("]")
		if "]" in fasta_spe :
			fasta_spe = "[" + fasta_spe

	aln_max_len = 0.4 # Coverage threshold
	
	# Extract species with their ids
	seq_id = []
	seq_spe = []
	seq_geneid = []

	for record in SeqIO.parse(ortho_file, "fasta") :
		if len(re.findall(r'\[([^]]*)\]$', record.description)) > 0 :
			seq_spe.append(re.findall(r'\[([^]]*)\]$', record.description)[0])
			seq_id.append(record.id)


	df = pd.DataFrame(list(zip(seq_id, seq_spe)),columns =['Identifier', 'Species'])
	
	# Merging the tables to add the species description to the first results of MMseqs2
	searchDB_species = pd.merge(searchDB_pandas,df,left_on = searchDB_pandas.iloc[:, 1], right_on="Identifier", how = "left")
	
	# Order and keep the best it for each species
	searchDB_species = searchDB_species.loc[(searchDB_species[7]-searchDB_species[6]+1)/fasta_length >= aln_max_len ] # Keep the sequences with a sufficient coverage
	searchDB_species = searchDB_species.sort_values(by = ['Species', 10,2], ascending=[True, True, False])
	searchDB_species = searchDB_species.drop_duplicates('Species', keep='first')
	searchDB_best_hits = searchDB_species.loc[searchDB_species['Species'] != fasta_spe ]
	
	kept_sequences = searchDB_best_hits[1].tolist()
	
	# Remove MULTISPECIES and isoform sequences from the final file
	file_content = ""
	for record in SeqIO.parse(ortho_file, "fasta") :
		if "MULTISPECIES" not in str(record.description) :
			if str(record.id) in kept_sequences :
				file_content = file_content + ">" + str(record.description) + "\n" + str(record.seq) + "\n"
		else :
			if str(fasta_name) in str(record.description) :
				file_content = file_content + ">" + str(record.description) + "\n" + str(record.seq) + "\n"
	outFile = open(ortho_file, 'w')
	print(file_content, file=outFile)
	outFile.close()
	ortho_file2 = results_path + "/" + str(record.name) + "_ortho2.fa"
	outFile = open(ortho_file2, 'w')
	print(file_content, file=outFile)
	outFile.close()
	
	# Prepare the back-database composed of the complete input species proteome
	print("\nPreparing the database for the back search")
	try :
		js_file = results_path + "/species_taxid.json"
		taxa_command = "ncbi-taxonomist map --names '%s' > %s" % (fasta_spe,js_file) # Using the tool for taxid retrieving
		subprocess.run(taxa_command, shell=True)
		with open(js_file) as f :
			js_content = f.read()
		js_dict = json.loads(js_content)
		ncbiTaxId = int(js_dict["taxon"]["taxid"])
	except :
		try :
			taxa_file = results_path + "/species_taxid.txt"
			speciesFile = open(taxa_file, "w")
			print(fasta_spe,file=speciesFile)
			speciesFile.close()
			taxa_ids = results_path + "/taxids_input.tsv" # File which will contain the taxids
			taxa_command = "taxaranks -i %s -o %s -t" % (taxa_file,taxa_ids) # Using the tool for taxid retrieving
			subprocess.run(taxa_command, shell=True)
			taxids_table = pd.read_csv(taxa_ids, sep='\t', header=0)
			ncbiTaxId = taxids_table.iloc[0 , -1] # species taxid
		except :
			print("Impossible to build a back-db with the input species")
			file_content = ">" + str(fasta_description) + "\n" + str(fasta_seq)
			outFile = open(ortho_file, 'w')
			print(file_content, file=outFile)
			outFile.close()
			return 0
	species_formated = fasta_spe.replace(" ","_").replace("(","_").replace(")","_").replace("/","_")
	input_refseq = results_path + "/" + species_formated + ".fa" # fasta file with the complete proteome
	input_refseq2 = results_path + "/" + species_formated + "2.fa" # fasta file with the complete proteome
	comp_db_command = "blastdbcmd -db %s -taxids %s -target_only -out %s" % (compdb,ncbiTaxId,input_refseq2)
	subprocess.run(comp_db_command, shell=True)
	file_content = ""
	if os.stat(input_refseq2).st_size == 0:
		print("Impossible to build a back-db with the actual database")
		file_content = ">" + str(fasta_description) + "\n" + str(fasta_seq)
		outFile = open(ortho_file, 'w')
		print(file_content, file=outFile)
		outFile.close()
		return 0

	# Remove MULTISPECIES sequences that can cause an error in the backsearch
	multispe_seq = []
	count = 0
	for record in SeqIO.parse(input_refseq2, "fasta") :
		if "MULTISPECIES" in str(record.description) :
			print("Avoiding to add " + str(record.id) + " to the database")
			multispe_seq.append(str(record.id))
		else :
			count += 1
			if count == 1 :
				outFile = open(input_refseq, 'w')
				print(">" + str(record.description) + "\n" + str(record.seq) + "\n", file=outFile)
				outFile.close()
			else :
				outFile = open(input_refseq, 'a')
				print(">" + str(record.description) + "\n" + str(record.seq) + "\n", file=outFile)
				outFile.close()
	
	backDB = results_path + "/backDB" # The fasta file converted to a MMseqs2 file
	backsearchDB = results_path + "/backsearchDB" # Result file of MMseqs2
	backsearchDB_converted = results_path + "/backsearchDB.m8" # Result file converted to an easy readable file
	tmp_dir = results_path + "/tmp" # Directory for temporary files
	backqueryDB = results_path + "/backqueryDB" # The fasta file converted to a MMseqs2 file
	
	# Conversion of the fasta file 
	createDB_command = "mmseqs createdb %s %s" % (input_refseq,backDB)
	subprocess.run(createDB_command, shell=True)
	final_fasta = ""
	# For each hit, run MMseqs2 against the input proteome
	for record in SeqIO.parse(ortho_file, "fasta") :
		tmp_fasta = results_path + "/tmp_fasta.fa"
		tmp_fasta_file = open(tmp_fasta, "w")
		tmp_fasta_content = ">" + str(record.description) + '\n' + str(record.seq) + '\n'
		tmp_fasta_len = len(record.seq)
		print(tmp_fasta_content ,file=tmp_fasta_file)
		print(str(record.description))
		tmp_fasta_file.close()
		createDB_command = "mmseqs createdb %s %s" % (tmp_fasta,backqueryDB)
		subprocess.run(createDB_command, shell=True)
		# Run the MMseqs search
		search_command = "mmseqs search %s %s %s %s -s 7 --threads %s" % (backqueryDB,backDB,backsearchDB,tmp_dir,cpus)
		subprocess.run(search_command, shell=True)
		# Conversion to an easy readable file
		convert_command = "mmseqs convertalis %s %s %s %s --threads %s" % (backqueryDB,backDB,backsearchDB,backsearchDB_converted,cpus)
		subprocess.run(convert_command, shell=True)
		try :
			backsearchDB_pandas = pd.read_csv(backsearchDB_converted, sep='\t', header=None)
			backsearchDB_pandas = backsearchDB_pandas.loc[(backsearchDB_pandas[7]-backsearchDB_pandas[6]+1)/tmp_fasta_len >= aln_max_len ] # Keep the sequences with a sufficient coverage
			backsearchDB_pandas = backsearchDB_pandas.sort_values(by = [10,2], ascending=[True, False])
			if backsearchDB_pandas.empty == False :
				#backsearchDB_pandas_id = backsearchDB_pandas[1][0]
				seq_geneid = []
				for index, row in backsearchDB_pandas.iterrows():
					mgq = "refseq:" + str(row[1])
					mg = mygene.MyGeneInfo()
					json_mg = mg.query(q = mgq, fields = "entrezgene")
					try :
						tmp_geneid = int(json_mg['hits'][0]['entrezgene'])
					except :
						tmp_geneid = None
					seq_geneid.append(tmp_geneid)
				backsearchDB_pandas['gene_ids'] = seq_geneid
				backsearchDB_pandas_id = int(backsearchDB_pandas["gene_ids"].iloc[0])
				if backsearchDB_pandas_id == geneid :
					final_fasta = final_fasta + tmp_fasta_content
		except :
			continue
	textfile = open(ortho_file, 'w')
	print(final_fasta, file = textfile)
	textfile.close()
	# Concatenate the principal fasta file with its orthologs
	textfile = open(ortho_file, 'r')
	filetext = textfile.read()
	textfile.close()
	if not re.findall(pattern, filetext) :
		add_file_to_another(fasta_file,ortho_file)
	return 0
	
if __name__ == "__main__":
	mmseqs_runner()
