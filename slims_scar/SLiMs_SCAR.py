#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Found structures of a list of motifs ##########

#### Import modules ####
import click
import subprocess
import os
from Bio.PDB import PDBList
import pandas as pd
from pdb_reader import pdb_parser
from split_rows import explode
from fasta2tab import fasta2tab
import csv
import warnings
from get_path import get_script_path
import urllib.parse
import urllib.request
import mygene

#### Setting variables ####

@click.command()
@click.option("-i","motif_file",type=str, required=True, show_default=True, help='tsv file containing a list of proteins and their comitifs')
@click.option("-o","output_dir",type=str,default="~/ELM_structures", show_default=True, help='Output directory to store the results')
def pdb_elm_alt(motif_file,output_dir) :
	'''
	SLiMs-SCAR allows to extract 2D and 3D representations of motifs from 
	evo-MOTiF results
	
	'''
	
	motif_file = os.path.abspath(motif_file)
	output_dir = os.path.abspath(output_dir)
	
	#### Set the path to the directory ####
	path_pipeline = get_script_path()
	os.chdir(path_pipeline)
	warnings.filterwarnings("ignore")
	
	#### Create directory ####
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)
	
	motifs = pd.read_csv(motif_file, sep='\t', header=0)
	motifs = motifs[motifs['UniProt'].notna()]
	
	uniprot_ids = [] # will contain the uniprot identifiers
	## Fill the list above with unique accessions
	for ids in motifs['UniProt']:
		if ids not in uniprot_ids :
			uniprot_ids.append(ids)
	query = ' '.join(uniprot_ids) # transform the list in query string for the conversion
	print("Converting to PDB ids\n")
	
	unipXpdb_file = output_dir + "/" + "UniprotXPDB.tsv" # file containing the corresponding uniprot/PDB IDs

	query_list = query.split(" ")
	unipXpdb = pd.DataFrame(columns = ['UniProt', 'PDB'])
	mg = mygene.MyGeneInfo()
	json_mg = mg.querymany(query_list, fields = "pdb",scopes = "uniprot")
	for prot in json_mg :
		q = prot["query"]
		if "pdb" in prot :
			prot_list = prot["pdb"]
			#print(prot_list)
			if isinstance(prot_list, list):
				for refel in prot_list :
					unipXpdb = unipXpdb.append({'UniProt' : q, 'PDB' : refel}, ignore_index = True)
			else :
				unipXpdb = unipXpdb.append({'UniProt' : q, 'PDB' : prot_list}, ignore_index = True)
	unipXpdb.to_csv(unipXpdb_file, sep = '\t', index=False, header = True) # print the new table to file
	
	#print(unipXpdb.head())
	motif_merged = pd.merge(motifs, unipXpdb, on="UniProt",how="left")
	#print(motif_merged.head())
	#print(len(motif_merged))
	motif_merged = motif_merged[motif_merged['UniProt'].notna()]
	motif_merged = motif_merged[motif_merged['PDB'].notna()]
	#print(len(motif_merged))
	structure_list = [] # list of the structure which will be extracted from the PDB files
	pdbs_list = [] # list of all the PDBs model names
	pdbs_file_list = [] # list with the name of the files which will be downloaded
	#### Fill the lists above ####
	for v in motif_merged['PDB']:
		pdbs_list.append(v)
		pdbs_file_list.append("pdb" + v.lower() + ".ent")
	
	#### Add the file names to the main data frame ####
	motif_merged["pdb_file"] = pdbs_file_list
	
	#### Download the PDBs files ####
	pdbl = PDBList()
	pdb_directory = output_dir + "/pdb_files"
	for ID in pdbs_list :
		pdbl.retrieve_pdb_file(ID, file_format="pdb",pdir =pdb_directory)
	structure_list = [] # list of the structure which will be extracted from the PDB files
	path_list = [] # list of the path to the PDB files
	motif_list = [] # list of the motifs 
	#### Fill the lists above ####
	for p in motif_merged["pdb_file"] :
		path_list.append(pdb_directory + "/" + p)
	for m in motif_merged["Sequence"] :
		motif_list.append(m)
	#### For each motif, create a file in which the representation will be ####
	png_dir = output_dir + "/png" # directory with the alignments of the motifs
	
	if not os.path.exists(png_dir):
		os.makedirs(png_dir)
	png_list = []
	c = 0
	for index, rows in motif_merged.iterrows():
		png = png_dir + "/" + rows["UniProt"] + "_" + rows["PDB"] + "_" + str(rows["Start_position"]) + "_" + str(rows["End_position"])
		png_list.append(png)
		c += 1
	motif_merged["PNG_file"] = png_list
	
	#### Same for a 3D representation of the motif ####
	pdb_shorten = []
	motifs_pdb_path = output_dir + "/" + "Motifs_PDB"
	if not os.path.exists(motifs_pdb_path):
		os.makedirs(motifs_pdb_path)
	for index, rows in motif_merged.iterrows():
		pdbs = motifs_pdb_path + "/" + rows["UniProt"] + "_" + rows["PDB"] + "_" + str(rows["Start_position"]) + "_" + str(rows["End_position"]) + ".pdb"
		pdb_shorten.append(pdbs)
	motif_merged["PDB_motif"] = pdb_shorten
	
	#### Looks for the structure for each motifs ####
	pdb_start_list = []
	pdb_end_list = []
	pdb_chain_list = []
	for i in range(len(png_list)) :
		if os.path.exists(path_list[i]) :
			l = pdb_parser(path_list[i],motif_list[i],png_list[i],pdb_shorten[i])
			#structure_list.append(pdb_parser(path_list[i],motif_list[i],png_list[i],pdb_shorten[i]))
			structure_list.append(l[0])
			pdb_start_list.append(l[1])
			pdb_end_list.append(l[2])
			pdb_chain_list.append(l[3])
		else :
			structure_list.append("")
			pdb_start_list.append("")
			pdb_end_list.append("")
			pdb_chain_list.append("")
		print("\n")
	motif_merged["structure"] = structure_list # column corresponding to the structure pattern
	motif_merged["pdb_start"] = pdb_start_list # column corresponding to the structure pattern
	motif_merged["pdb_end"] = pdb_end_list # column corresponding to the structure pattern
	motif_merged["pdb_chain"] = pdb_chain_list # column corresponding to the structure pattern
	
	#### Delete rows for which no structure was found ####
	motif_merged = motif_merged[motif_merged['structure'] != ""]
	
	#### Remove the path from the PNG and PDB files to make it more readable ####
	png_shorten = []
	for files in motif_merged["PNG_file"] :
		png_shorten.append(os.path.basename(files) + ".png")
	motif_merged["PNG_file"] = png_shorten
	pdb_shorten = []
	for files in motif_merged["PDB_motif"] :
		pdb_shorten.append(os.path.basename(files))
	motif_merged["PDB_motif"] = pdb_shorten
	motif_merged = motif_merged[["UniProt","SLiM","Sequence","structure","Start_position","End_position","PDB","pdb_start","pdb_end","pdb_chain","PNG_file","PDB_motif"]]
	print(motif_merged.head())
	output_file = output_dir + "/STRUCTURE.tsv"
	motif_merged.to_csv(output_file,header = True, sep = "\t", quoting=csv.QUOTE_NONE, index=False)
	
if __name__ == "__main__":
	pdb_elm_alt()
