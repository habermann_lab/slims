# SLiMs-SCAR



## What is this tool ?

SLiMs-SCAR (**S**hort **Li**near **M**otif**s** **S**tru**C**ture extr**A**cto**R**) aimed at extracting the coded structure of SLiMs (**S**hort **Li**near **M**otif**s**) from PDB files which are downloaded by the script.

## How to run

/!\ SLiMs-SCAR is a part of the evo-MOTiF project. Here, we assumed you want to install evo-MOTiF and the RefSeq Database in your **home folder** and you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your computer.

#### Installation

First, install evo-MOTiF on your computer :

`git clone https://gitlab.com/habermann_lab/slims.git`

Then, go in the evo-MOTiF folder :

`cd slims/slims_scar`

A YAML file is available to facilitate the installation using conda environment.

`conda env create -f SCAR.yml -n SCAR`

The package mygene may be needed to be installed by an other way as conda seems to find conflicts while trying to install it. So please, run : 

`pip install -e git+https://github.com/biothings/mygene.py#egg=mygene`

#### Running SLiMs-SCAR

This tool run with Python 3.7 with the command :
`python3 ~/slims/slims_scar/SLiMs_SCAR.py -i MOTIF_FILE -o OUTPUT_DIR`

where :
- `MOTIF_FILE` is a tsv file containing the results of an evo-MOTiF run
- `OUTPUT_DIR` is the output directory to store the results

## Results

The resulting files are `STRUCTURE.tsv`in which you can find the information extracted about the structures of your motifs, and two directories `Motifs_PDB` containing PDB files of your motifs and `png` which contains 2D representations of the motifs.

The structure is represented by a letter code which is basically the one used in the PDB files :

| Code  | Structure                                 |
| :------: | :------------------------------------: |
| H        | Alpha helix (4-12)                   |
| B        | Isolated beta-bridge residue  | 
| E         | Strand                                     |
| G        | 3-10 helix                                |
| I          | Pi helix                                    |
| T         | Turn                                         |
| S         | Bend                                        |
| N        | None                                        |

## Dependencies

#### Languages
- Python 3.7

#### Python 3 modules
- click
- Bio (biopython)
- csv
- pandas
- subprocess
- os
- numpy
- biotite
- matplotlib
- csv
- warnings
- mygene