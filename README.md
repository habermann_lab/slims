# evo-MOTiF



## What is this tool ?

evo-MOTiF aimed at studying the localisation and the conservation of SLiMs (**S**hort **Li**near **M**otif**s**) in an input sequence. The search of orthologs in the first part can help to study the  evolution of SLiMs and their effects on the protein.

## How to run

/!\ Here, we assumed you want to install evo-MOTiF and the RefSeq Database in your **home folder** and you have [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed on your computer.

/!\ To use SLiMs-SCAR, please refer to the README in the directory called `slims_scar`.

#### Installation of the conda environment and the dependencies

First, install evo-MOTiF on your computer :

`git clone https://gitlab.com/habermann_lab/slims.git`

Then, add the IUPRED path to your .bashrc :

`echo "export IUPred_PATH=~/slims/iupred" >> ${HOME}/.bashrc`

Entrez Direct : E-utilities from the NCBI can be found [here](https://www.ncbi.nlm.nih.gov/books/NBK179288/). Run the following command and follow the instructions they will give you in order to install it :

`sh -c "$(wget -q ftp://ftp.ncbi.nlm.nih.gov/entrez/entrezdirect/install-edirect.sh -O -)"` 

To make these change effective, enter : 

`source ~/.bashrc`

Then, go in the evo-MOTiF folder :

`cd slims`

It is recommended to use a miniconda environment which is included in the pipeline by simply running in the terminal :

`conda env create -f environment.yml -n MOTiF`

Then, you activate it by writing :

`conda activate MOTiF`.

Finally, you'll need to install the remaining R packages, which seem to not be able to be in the conda environment (this step can be quite long, please be patient) : 

`Rscript install_packages.R`

The packages taxonomy_ranks, ncbi-taxonomist and mygene may be needed to be installed by an other way as conda seems to find conflicts while trying to install it. So please, run : 

`pip install taxonomy_ranks`

`pip install ncbi-taxonomist`

`pip install -e git+https://github.com/biothings/mygene.py#egg=mygene`

To install SLiMSuite, simply use : 

`git clone https://github.com/slimsuite/SLiMSuite.git`

By doing that, you will only need to install 
- BLAST (instructions [here](https://www.ncbi.nlm.nih.gov/books/NBK569861/))
- PhyML (instructions [here](https://github.com/stephaneguindon/phyml))

#### Database for homology search

Prior to run evo-MOTiF, you will need to install the RefSeq database. We provide a file containing the links to the ftp server of the NCBI to download it but be careful that he can be not up to date.

`mkdir ~/refseq_db`

`cd refseq_db`

`wget -i ~/slims/refseq_db_links.txt`

If you want to use MMseqs2 as an orthologs searcher, you'll need to first convert RefSeq to a compatible database. For that, you can use these command lines :

`blastdbcmd -db ~/refseq_db/refseq_protein -entry all > ~/refseq_db/refseq.fa`

`mkdir ~/MMseqs_refseq_db`

`mmseqs createdb ~/refseq_db/refseq.fa ~/MMseqs_refseq_db/refseq`

## Single sequence mode

This tool run with Python 3.7 with the command :

`python3 ~/slims/evo_MOTiF.py FASTA_FILE RESULTS_PATH -db REFSEQ_PATH -cdb SEC_REFSEQ_PATH -o ORTHOLOGY -p PROBA_FILTER -m ELM_MOTIFS -t TREE -c CPUS -s TAXID -mail YOUR.MAIL@MAIL.COM -ro REDO_ORTHO -eo ORTHOLOGY2 -cons CONS_THRES -spe SPE_THRES -dis DIS_THRES -acc ACC_THRES`
 
where :
- `FASTA_FILE` is a file containing one sequence from refseq
- `RESULTS_PATH` is the path to the directory where you want to results to be
- `-db REFSEQ_PATH` is the path to the refseq database
- `-cdb SEC_REFSEQ_PATH` is a path to the refseq blast-formated database (mandatory only with `-o mmseqs2`)
- `-o ORTHOLOGY` is a string, by default set to BLAST. It corresponds to the orthology method you want to use. There is three options : `blast`, `orthodb` or `mmseqs2`
- `-p PROBA_FILTER` is the probability of occurence of a SLiMs, by default it is set to None, there is no filtration
- `-m ELM_MOTIFS` is a list of motif, you can use the file `elm_classe.tsv` found in the pipeline directory
- `-t TREE` is a string, by default it is None, it does not run a tree construction, there is two options, `iqtree` or `phyml`
- `-c CPUS` : number of CPUs to use for the orthologs search and for IQTree
- `-s TAXID` : taxid to limit the BLAST search (default = `2759` : eukaryota)
- `-mail YOUR.MAIL@MAIL.COM` : enter your mail to allow the download of sequences (mandatory with `-o mmseqs2`)
- `-ro REDO_ORTHO` : set `False` if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein
- `-eo ORTHOLOGY2` is a string, by default set to `None`. It corresponds to the orthology method you want to use if OrthoDB did not worked. There is two options : `blast` or `mmseqs2`
- `-cons CONS_THRES` is a number corresponding to the desired threshold between a positional conserved or not conserved motif.
- `-spe SPE_THRES` is a number corresponding to the desired threshold to define an overall conserved motif.
- `-dis DIS_THRES` is a number corresponding to the desired threshold between an ordered or a disordered motif.
- `-acc ACC_THRES` is a number corresponding to the desired threshold to define an exposed motif.

This tool enables to study the conservation and the order of SLiMs found in an input sequence.
	
The results can be found in the directories `RESULTS_PATH/graph` and `RESULTS_PATH/table`.

The tree file is the file with the extension : `.phy_phyml_tree.txt`for PhyML or `.phy.treefile` for IQTree. A PNG file is created according to it but it is a raw file. With the text file, you can manipulate it and modify it.

## Batch mode

This tool run with Python 3.7 with the command :

`python3 ~/slims/Batch_MOTiF.py FASTA RESULTS_PATH -db REFSEQ_PATH -cdb SEC_REFSEQ_PATH -o ORTHOLOGY -p PROBA_FILTER -m ELM_MOTIFS -t TREE -c CPUS -s TAXID  -mail YOUR.MAIL@MAIL.COM -ro REDO_ORTHO -eo ORTHOLOGY2 -cons CONS_THRES -spe SPE_THRES -dis DIS_THRES -acc ACC_THRES`

where :
- `FASTA` is a file containing multiple sequences from refseq
- `RESULTS_PATH` is the path to the directory where you want to results to be
- `-db REFSEQ_PATH` is the path to the refseq database
- `-cdb SEC_REFSEQ_PATH` is a path to the refseq blast-formated database (mandatory only with `-o mmseqs2`)
- `-o ORTHOLOGY` is a string, by default set to BLAST. It corresponds to the orthology method you want to use. There is three options :  `blast`, `orthodb` or `mmseqs2`
- `-p PROBA_FILTER` is the probability of occurence of a SLiMs, by default it is set to None, there is no filtration
- `-m ELM_MOTIFS` is a list of motif, you can use the file `elm_classe.tsv` found in the pipeline directory
- `-t TREE` is a string, by default it is None, it does not run a tree construction, there is two options, `iqtree` or `phyml`
- `-c CPUS` : number of CPUs to use for the orthologs search and for IQTree
- `-s TAXID` : taxid to limit the BLAST search (default = `2759` : eukaryota)
- `-mail YOUR.MAIL@MAIL.COM` : enter your mail to allow the download of sequences (mandatory with `-o mmseqs2`)
- `-ro REDO_ORTHO` : set `False` if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein
- `-eo ORTHOLOGY2` is a string, by default set to `None`. It corresponds to the orthology method you want to use if OrthoDB did not worked. There is two options : `blast` or `mmseqs2`
- `-cons CONS_THRES` is a number corresponding to the desired threshold between a positional conserved or not conserved motif.
- `-spe SPE_THRES` is a number corresponding to the desired threshold to define an overall conserved motif.
- `-dis DIS_THRES` is a number corresponding to the desired threshold between an ordered or a disordered motif.
- `-acc ACC_THRES` is a number corresponding to the desired threshold to define an exposed motif.

The results can be found in the directories `RESULTS_PATH/SEQ_ID/graph` and `RESULTS_PATH/SEQ_ID/table` where `SEQ_ID` is a directory named after the RefSeq ID of the protein. A sum up file containing the results for all the protein is found at `RESULTS_PATH/Batch_results.tsv`.


## EXP-MOTiF

This tool is a derived version on evo-MOTiF which used datasets from different databases :
- [PhosphoSitePlus](https://www.phosphosite.org/homeAction)
- [elm.eu.org](http://elm.eu.org/)
- XLMS. This one is named according to a cross-linking associated with mass spectroscopy but it take into account tsv files with two columns, Uniprot ids and motifs sequences.

It runs with the command :

`python3 ~/slims/EXP_MOTiF.py INPUT_FILE RESULTS_PATH -db REFSEQ_PATH -cdb SEC_REFSEQ_PATH -dt DATA_TYPE -m MOTIFS -o ORTHOLOGY -t TREE -c CPUS -s TAXID  -mail YOUR.MAIL@MAIL.COM -ro REDO_ORTHO -eo ORTHOLOGY2 -cons CONS_THRES -spe SPE_THRES -dis DIS_THRES -acc ACC_THRES`

where :
- `INPUT_FILE` is a file containing a dataset from phosphosite plus
- `RESULTS_PATH` is the path to the directory where you want to results to be
- `-db REFSEQ_PATH` is the path to the refseq database
- `-cdb SEC_REFSEQ_PATH` is a path to the refseq blast-formated database (mandatory only with `-o mmseqs2`)
- `-dt DATA_TYPE` : type of the dataset (`elm`, `psp` or `xlms`)
- `-m MOTIFS` is a list of motif (for ELM datasets only)
- `-o ORTHOLOGY` is a string, by default set to BLAST. It corresponds to the orthology method you want to use. There is two options : `blast`, `orthodb` or `mmseqs2`
- `-t TREE` is a string, by default it is None, it does not run a tree construction, there is two options, `iqtree` or `phyml`
- `-c CPUS` : number of CPUs to use for the orthologs search and for IQTree
- `-s TAXID` : taxid to limit the BLAST search (default = `2759` : eukaryota)
- `-mail YOUR.MAIL@MAIL.COM` : enter your mail to allow the download of sequences
- `-ro REDO_ORTHO` : set `False` if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein
- `-eo ORTHOLOGY2` is a string, by default set to `None`. It corresponds to the orthology method you want to use if OrthoDB did not worked. There is two options : `blast` or `mmseqs2`
- `-cons CONS_THRES` is a number corresponding to the desired threshold between a positional conserved or not conserved motif.
- `-spe SPE_THRES` is a number corresponding to the desired threshold to define an overall conserved motif.
- `-dis DIS_THRES` is a number corresponding to the desired threshold between an ordered or a disordered motif.
- `-acc ACC_THRES` is a number corresponding to the desired threshold to define an exposed motif.

The results can be found in the directories `RESULTS_PATH/SEQ_ID/graph` and `RESULTS_PATH/SEQ_ID/table` where `SEQ_ID` is a directory named after the RefSeq ID of the protein. A sum up file containing the results for all the protein is found at `RESULTS_PATH/"DATA_TYPE"_results.tsv`.

## Dependencies

#### Languages
- Python 3.7
- Python 2.7
- R version 3.6

#### Tools
- MAFFT version 7
- BLAST 2.10.0
	/!\ Make sure that BLAST is in usr/bin
- Entrez Direct : E-utilities from the NCBI
- PhyML
- IQTree version 2
- slimsuite
	/!\ installed in the same directory of the pipeline
- iupred version 1
	/!\ installed it in the same directory of the pipeline
- MMseqs2

#### Python 3 modules
- click
- Bio (biopython)
- matplotlib
- Entrez
- subprocess
- os
- pandas
- taxonomy-ranks
- ncbi-taxonomist
- mygene
- ete3

#### R packages
- ggplot2
- tidyr
- optparse
- seqinr

## Important informations

#### RefSeq database

Please install the refseq protein database to run the ortholog search and ake sure that your input file has a refseq ID

*example :* `>NP_001179.1 bcl-2 homologous antagonist/killer [Homo sapiens]`

If you do not respect this, you will not have informations about the conservation.

#### MMseqs2 RefSeq database

To use MMseqs2, you will need to convert the database to a MMseqs2 database. For that, you can use these command lines :

`blastdbcmd -db PATH/TO/refseq_protein -entry all > refseq.fa`

`mmseqs createdb refseq.fa refseq`
	
#### IUPred
IUPred is included in the files of the pipeline. To work, it needs a environment path.
Add this command to your `~/.bashrc` :

`export IUPred_PATH=~/slims_pipeline/iupred`

If you do not do this, you will not have informations about the order.

#### Motif file

By default, the file `elm_classes.tsv` is used but you can personnalise
your search by adding your own list.
Be careful of the file format in `.tsv`:

`Name	Sequence	#Comments`

where sequence is a regex of your motif.
Everything after # is ignored
