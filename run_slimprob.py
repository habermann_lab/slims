#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Runner for SlimProb ##########
# The first step is not useful for the pipeline but was used in other works using a lot of sequences

#### Import modules ####
import click
import subprocess
import os

@click.command()
@click.argument("fasta_file")
@click.argument("outpath")
@click.argument("num_lines")
@click.argument("slimprob_file")
@click.argument("elm_motifs")
@click.argument("alignments_path")
def compute_slimprob(fasta_file, outpath, num_lines,slimprob_file,elm_motifs,alignments_path) :
	'''
	fasta_file : file to split and analyse
	
	outpath : pathway to the results of slimprob
	
	num_lines : number of sequences wanted in subfiles 
		max 1000, multiple of 2, if not, it will not work
	
	slimprob_file : slimprob results concatenated
	
	elm_motifs : files containing the elm motifs
	
	alignments_path : path where to find the aligments for the
		conservation calculation
	
	Split the fasta file and run slimprob on each subfile because
	slimprob can't be ran on too much sequences
	'''
	
	print("We are going to run SlimProb for you ;)")
	num_lines =int(num_lines)
	filesList = list()
	count_sequences = 0
	
	#### Creating the result directory ####
	if not os.path.exists(outpath):
		os.makedirs(outpath)
	
	#### Reading the fasta file ####
	with open(fasta_file,"r") as inFile :
		text = ""
		fileCount = 0
		
		#### For the sequences in the input, it creates files with num_lines sequences ####
		for line in inFile :
			if line.startswith('>') :
				if count_sequences == num_lines :
					name = outpath + "/fasta_" + str(fileCount) + ".fasta"
					filesList.append(name)
					with open(name,"w") as outfile :
						outfile.write(text)
					fileCount += 1
					count_sequences = 0
					text = ""
				count_sequences += 1
			text += line
			
		# It creates the last file with the remaining sequences
		name = outpath + "/fasta_" + str(fileCount) + ".fasta"
		filesList.append(name)
		with open(name,"w") as outfile :
			outfile.write(text)
			
	print("Your file was splited in " + str(fileCount+1) + " files.")
	
	count = 0
	slimprobList = list()
	
	#### For each file, SlimProb is run ####
	for files in filesList :
		outputDirectory = files.rstrip(".fasta") + "_slimprob" + str(count)
		output_file = outputDirectory + "/" + "slimprob_" + str(count) + ".tsv"
		bashcommand1 = "python2 SLiMSuite/tools/slimprob.py motifs=%s seqin=%s walltime=1000 resfile=%s resdir=%s blastpath=/usr/bin blast+path=/usr/bin masking=T dismask=F iuchdir=F iupath=iupred/iupred iucut=0.2 slimcalc=IUP,Comp,Cons,SA conscore=all usealn=T alndir=%s alnext=align alngap=T maxsize=10000000000 maxseq=10000000 " % (elm_motifs,files,output_file,outputDirectory,alignments_path)
		count += 1
		subprocess.run(bashcommand1,shell=True)
		slimprobList.append(output_file.rstrip(".tsv") + ".occ.tsv")
	
	#### All results file are concatenated and deleted ####
	outputSlim = open(outpath + "/" + slimprob_file,"w")
	count = 0
	for slimFile in slimprobList :
		spfile = open(slimFile,"r")
		lines=spfile.readlines()
		if count == 0 :
			for line in lines :
				outputSlim.write(line)
		else :
			for line in lines[1:] :
				outputSlim.write(line)
		spfile.close()
		count += 1
	outputSlim.close()
	
	files_list = os.listdir(outpath)
	for item in files_list:
		if item.endswith(".fasta"):
			os.remove(os.path.join(outpath, item))

if __name__ == "__main__":
	compute_slimprob()
