#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Pipeline to run a complete batch SLiM analysis for a set of input sequences ##########

#### Import modules ####
import click
import subprocess
import os
from Bio import SeqIO
from get_path import get_script_path
from evo_MOTiF import run_pipeline

#### Setting variables ####

@click.command()
@click.argument("fasta")
@click.argument("results_path")
@click.option("-db","refseq_path",type=str,default=None, show_default=True, help='path to the refseq database')
@click.option("-o","orthology",type=str,default="blast", show_default=True, help='the orthology method to use (blast, orthodb or mmseqs2)')
@click.option("-p","proba_filter",type=float,default=1, show_default=True, help='a threshold to remove SLiMs with a too high value of occurence')
@click.option("-m","elm_motifs",type=str,default=None, show_default=True, help='a list of motifs\nBy default, the file elm_classes.tsv is used but you can personnalise your search by addind your own list.')
@click.option("-t","tree",type=str,default=None, show_default=True, help='whether to run or not PhyML or IQTree to make a tree with the orthologs found (phyml or iqtree')
@click.option("-c","cpus",type=int,default=8, show_default=True, help='number of CPUs to use for the orthologs search and for IQTree)')
@click.option("-s","taxid",type=int,default=2759, show_default=True, help='taxid to limit the BLAST search (2759 = eukaryota)')
@click.option("-mail","user_mail",type=str,default=None, show_default=True, help='enter your mail to allow the download of sequences (mandatory is your using "-o mmseqs2")')
@click.option("-ro","redo_ortho",type=bool,default=True, show_default=True, help='Set "False" if you want the pipeline to look on your computer if an orthologs searcher has been already done for this protein')
@click.option("-eo","emergency_ortho",type=str,default=None, show_default=True, help='the orthology method to use (blast or mmseqs2) if orthodb did not worked')
@click.option("-cons","cons_thres",type=float,default=0.5, show_default=True, help='threshold for positional conservation score')
@click.option("-spe","spe_thres",type=float,default=0.66, show_default=True, help='threshold for overall conservation score')
@click.option("-dis","dis_thres",type=float,default=0.5, show_default=True, help='threshold for disorder score')
@click.option("-acc","acc_thres",type=float,default=6, show_default=True, help='threshold for surface accessibility score')
@click.option("-cdb","compdb",type=str,default=None, show_default=True, help='path to the refseq BLAST_formatted database')
@click.pass_context
def run_batch(ctx,fasta,results_path,refseq_path,orthology,proba_filter,elm_motifs,tree,cpus,taxid,user_mail,redo_ortho,emergency_ortho,cons_thres,spe_thres,dis_thres,acc_thres,compdb) :
	'''
	FASTA : file containing the input sequences
	
		/!\ refseq format only
	
		example : >NP_001179.1 bcl-2 homologous antagonist/killer [Homo sapiens]
	
	
	RESULTS_PATH : path to the directory where you want to results to be
	
	This tool enables to study the conservation and the order of SLiMs
	found in a set of  input sequences.
	
	The results can be found in the directories "RESULTS_PATH"/"SEQ_ID"/graph and "RESULTS_PATH"/"SEQ_ID"/table	
	'''
	fasta = os.path.abspath(fasta)
	results_path = os.path.abspath(results_path)
	if refseq_path != None :
		refseq_path = os.path.abspath(refseq_path)
	if compdb != None :
		compdb = os.path.abspath(compdb)
	if elm_motifs != None :
		elm_motifs = os.path.abspath(elm_motifs)
		
	#### Set the path to the directory ####
	path_pipeline = get_script_path()
	os.chdir(path_pipeline)
	
	#### Error message if the value entered is not good ####
	if proba_filter == None :
		pass
	elif proba_filter<0 or proba_filter>1 :
		print("A probability value is between 0 and 1.\nPlease, retry with a correct value !")
		return 0
	if orthology not in ["blast","orthodb","mmseqs2"] :
		print("Please enter a correct orthology method")
		return 0
	if orthology == "blast" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if tree not in ["iqtree","phyml",None,"None"] :
		print("Please, enter a correct value for the tree method")
		return 0
	if orthology == "mmseqs2" and refseq_path == None :
		print("Please, don't forget to define a path to a refseq database")
		return 0
	if orthology == "mmseqs2" and user_mail == None :
		print("Please, don't forget to specify your mail while using MMseqs2")
		return 0
	if type(redo_ortho) != type(True) :
		print("-ro must be True or False, please re-set a correct value")
		return 0
	if emergency_ortho not in [None,"blast","mmseqs2"] :
		print("Please enter a correct secondary orthology method")
		return 0
	if orthology == "mmseqs2" and compdb == None :
		print("Please, don't forget to set the path to the BLAST-formatted database while using MMseqs2")
		return 0
	if elm_motifs == None :
		print("Please, don't forget to set the path to a motifs list")
		return 0
	count_sequences = 0
	filesList = list()
	
	#### Creation of the RESULTS_PATH directories ####
	path_fastas = results_path + "/fastas"
	if not os.path.exists(results_path):
		os.makedirs(results_path)
	if not os.path.exists(path_fastas):
		os.makedirs(path_fastas)
	
	#### Reading the in put file to put one sequence per file ####
	with open(fasta,"r") as inFile :
		text = ""
		fileCount = 0
		
		#### For the sequences in the input, it creates files with 1 sequence ####
		for line in inFile :
			if line.startswith('>') :
				if count_sequences > 0 :
					name = str(path_fastas) + "/" + str(count_sequences) + ".fasta"
					filesList.append(name)
					with open(name,"w") as outfile :
						outfile.write(text)
					text = ""
				count_sequences += 1
			text += line
		name = str(path_fastas) + "/" + str(count_sequences) + ".fasta"
		filesList.append(name)
		with open(name,"w") as outfile :
			outfile.write(text)
			
	counter = 0
	#### For all the files, run the pipeline ####
	for files in filesList :
		record = SeqIO.read(files, "fasta")
		output_path = results_path + "/" + str(record.name)
		ctx.invoke(run_pipeline, fasta_file = files, results_path = output_path,orthology = orthology, elm_motifs = elm_motifs, tree = tree, cpus = cpus, taxid = taxid,refseq_path = refseq_path, user_mail = user_mail, redo_ortho = redo_ortho, emergency_ortho = emergency_ortho,proba_filter = proba_filter,cons_thres = cons_thres,spe_thres = spe_thres,dis_thres = dis_thres,acc_thres = acc_thres, compdb = compdb)
		if counter == 0 :
			header = output_path
		counter += 1
	
	#### Concatenation of all the results in one tsv file
	output_file = results_path + "/Batch_results.tsv"
	all_path = results_path + "/*"
	
	## Retrieve the header of the file in the first result
	header_command = "head -1 %s/table/MOTiF_results.tsv > %s" % (header,output_file)
	subprocess.run(header_command,shell=True)
	## Concatenate all the results in this file (header not counted)
	concatenate_command = "tail -n +2 -q %s/table/MOTiF_results.tsv >> %s" % (all_path,output_file)
	subprocess.run(concatenate_command,shell=True)
	
	#### Concatenation of the filtered results in one tsv file
	output_file_filtered = results_path + "/Batch_results_filtered.tsv"
	if proba_filter == 1 :
		proba_filter = None
	if proba_filter != None :
		## Retrieve the header of the file in the first result
		header_command = "head -1 %s/table/slimprob_results_filtered.tsv > %s" % (header,output_file_filtered)
		subprocess.run(header_command,shell=True)
		## Concatenate all the results in this file (header not counted)
		concatenate_command = "tail -n +2 -q %s/table/slimprob_results_filtered.tsv >> %s" % (all_path,output_file_filtered)
		subprocess.run(concatenate_command,shell=True)
			
if __name__ == "__main__":
	run_batch()
