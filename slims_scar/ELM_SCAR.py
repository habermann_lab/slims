#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Found structures of a list of motifs ##########

#### Import modules ####
import click
import subprocess
import os
from Bio.PDB import PDBList
import pandas as pd
from pdb_reader import pdb_parser
from split_rows import explode
from fasta2tab import fasta2tab
import csv
import warnings
from get_path import get_script_path

#### Setting variables ####

@click.command()
@click.option("-i","elm_instances",type=str, required=True, show_default=True, help='ELM instances tsv file')
@click.option("-ft","fasta_instances",type=str, required=True, show_default=True, help='ELM instances fasta')
@click.option("-o","output_dir",type=str,default="~/ELM_structures", show_default=True, help='Output directory to store the results')
def pdb_elm(elm_instances,fasta_instances,output_dir) :
	
	#### Set the path to the directory ####
	path_pipeline = get_script_path()
	os.chdir(path_pipeline)
	warnings.filterwarnings("ignore")
	
	#### Create directory ####
	directory_command = "mkdir -p %s" % (output_dir)
	subprocess.run(directory_command, shell=True)
	
	#### Removing the comment lines at the beginning of the input files ####
	elm_instances_cut = output_dir + "/" + "elm_instances_cut.tsv"
	remove_lines_command = "sed '/^#/ d' < %s > %s" % (elm_instances,elm_instances_cut)
	subprocess.run(remove_lines_command, shell=True)
	
	#### Load the elm instances tsv file in a pandas data frame ####
	elm_instance_pandas = pd.read_csv(elm_instances_cut, sep='\t', header=0)
	
	#### Convert the fasta to tab ####
	fasta_tab_instances = output_dir + "/" + os.path.basename(fasta_instances).rstrip(".fasta") + "_fasta.tsv"
	fasta2tab(fasta_instances,fasta_tab_instances)
	
	#### Load the fasta instances tab to a pandas dataframe ####
	elm_fasta_pandas = pd.read_csv(fasta_tab_instances, sep = '\t', header=0)
	
	#### Delete rows which are not associated with a 3D model ####
	elm_instance_pandas = elm_instance_pandas[elm_instance_pandas['PDB'].notna()]
	
	#### Merge the 2 data frames to add the protein sequence to the main data frame ####
	elm_merged = pd.merge(elm_instance_pandas, elm_fasta_pandas, on="Primary_Acc",how="left")
	
	#### Cut the rows with different 3D model IDs in different rows ####
	elm_exploded = explode(elm_merged.assign(PDB=elm_merged.PDB.str.split(' ')), 'PDB')
	#print(elm_exploded.head())
	
	pdbs_list = [] # list of all the PDBs model names
	pdbs_file_list = [] # list with the name of the files which will be downloaded
	#### Fill the lists above ####
	for v in elm_exploded['PDB']:
		pdbs_list.append(v)
		pdbs_file_list.append("pdb" + v.lower() + ".ent")
	
	#### Add the file names to the main data frame ####
	elm_exploded["pdb_file"] = pdbs_file_list
	
	#### Download the PDBs files ####
	pdbl = PDBList()
	pdb_directory = output_dir + "/pdb_files"
	for ID in pdbs_list :
		pdbl.retrieve_pdb_file(ID, file_format="pdb",pdir =pdb_directory)
		
	structure_list = [] # list of the structure which will be extracted from the PDB files
	start_list = [] # list of all the start positions of the motifs
	end_list = [] # list of all the end positions of the motifs
	path_list = [] # list of the path to the PDB files
	seq_list = [] # list of the sequences
	motif_list = [] # list of the motifs 
	#### Fill the lists above ####
	for s in elm_exploded["Start"] :
		start_list.append(s)
	for e in elm_exploded["End"] :
		end_list.append(e)
	for p in elm_exploded["pdb_file"] :
		path_list.append(pdb_directory + "/" + p)
	for m in elm_exploded["Sequence"] :
		seq_list.append(m)
		
	#### For each sequence, extract the motif sequence ####
	for j in range(len(seq_list)) :
		st = int(start_list[j])
		ed = int(end_list[j])
		motif_list.append(seq_list[j][st-1:ed])
	
	#### For each motif, create a file in which the representation will be ####
	png_dir = output_dir + "/png" # directory with the alignments of the motifs
	directory_command = "mkdir -p %s" % (png_dir)
	subprocess.run(directory_command, shell=True)
	png_list = []
	for index, rows in elm_exploded.iterrows():
		png = png_dir + "/" + rows["Accession"] + "_" + rows["PDB"]
		png_list.append(png)
	elm_exploded["PNG_file"] = png_list
	
	#### Same for a 3D representation of the motif ####
	pdb_shorten = []
	motifs_pdb_path = output_dir + "/" + "Motifs_PDB"
	directory_command = "mkdir -p %s" % (motifs_pdb_path)
	subprocess.run(directory_command, shell=True)
	for index, rows in elm_exploded.iterrows():
		pdbs = motifs_pdb_path + "/" + rows["Accession"] + "_" + rows["PDB"] + ".pdb"
		pdb_shorten.append(pdbs)
	elm_exploded["PDB_motif"] = pdb_shorten

	#### Looks for the structure for each motifs ####
	for i in range(len(start_list)) :
		if os.path.exists(path_list[i]) :
			structure_list.append(pdb_parser(path_list[i],motif_list[i],png_list[i],pdb_shorten[i]))
		else :
			structure_list.append("")
		print("\n")
	
	#### Add the new columns to the data frame ####
	elm_exploded["motif_seq"] = motif_list # column corresponding to the motif sequence
	elm_exploded["structure"] = structure_list # column corresponding to the structure pattern
	
	#### Delete rows for which no structure was found ####
	elm_final = elm_exploded[elm_exploded['structure'] != ""]
	
	#### Print the new table to a file ####
	final_file = output_dir + "/OUTPUT.tsv"
	elm_final.to_csv(final_file,header = True, sep = "\t", quoting=csv.QUOTE_NONE, index=False)
	
	#### Select only the interesting colums ####
	short_file = output_dir + "/STRUCTURE.tsv"
	elm_short = elm_final[["Accession","ELMIdentifier","Primary_Acc","PDB", "Start", "End", "motif_seq","structure","PNG_file","PDB_motif"]]
	elm_short.to_csv(short_file,header = True, sep = "\t", quoting=csv.QUOTE_NONE, index=False)
	png_shorten = []
	
	#### Remove the path from the PNG and PDB files to make it more readable ####
	for files in elm_short["PNG_file"] :
		png_shorten.append(os.path.basename(files) + ".png")
	elm_short["PNG_file"] = png_shorten
	pdb_shorten = []
	for files in elm_short["PDB_motif"] :
		pdb_shorten.append(os.path.basename(files))
	elm_short["PDB_motif"] = pdb_shorten
	
	elm_list = []
	struc_dir = output_dir + "/fasta_motifs" # directory with fasta of the motifs
	directory_command = "mkdir -p %s" % (struc_dir)
	subprocess.run(directory_command, shell=True)
	align_dir = output_dir + "/alignments" # directory with the alignments of the motifs
	directory_command = "mkdir -p %s" % (align_dir)
	subprocess.run(directory_command, shell=True)
	results_dir = output_dir + "/RESULTS" # results directory
	directory_command = "mkdir -p %s" % (results_dir)
	subprocess.run(directory_command, shell=True)
	
	#### List of all motif types ####
	for e in elm_short["ELMIdentifier"] :
		if e not in elm_list :
			elm_list.append(e)
			
	#### For each motif, extract the data ####
	for elm in elm_list :
		newdf = elm_short[elm_short["ELMIdentifier"] == elm] # sub-table with one type of motif
		file_motif = struc_dir + "/" + elm + ".fasta"
		f = open(file_motif, "w")
		acc_list = []
		
		#### Add the sequence of each motif to a fasta file ####
		for index, rows in newdf.iterrows():
			if rows["Accession"] not in acc_list :
				print(">" + rows["Accession"] + "\n" + rows["motif_seq"], file = f)
				acc_list.append(rows["Accession"])
		f.close()
		align_file = align_dir + "/" + elm + ".align"
		
		#### Multiple sequence alignment of the motifs ####
		align_command = "yes | mafft %s > %s" % (file_motif, align_file)
		subprocess.run(align_command, shell=True)
		
		#### Convert the fasta to tab ####
		fasta_tab_align = align_dir + "/" + os.path.basename(align_file).rstrip(".align") + "_align.tsv"
		fasta2tab(align_file,fasta_tab_align)
		
		#### Add the aligned sequences to the pandas dataframe ####
		tab_align_pandas = pd.read_csv(fasta_tab_align, sep = '\t', header=0) # tsv file with the alignments
		tab_merged = pd.merge(newdf, tab_align_pandas, left_on="Accession", right_on = "Primary_Acc",how="left")
		tab_merged = tab_merged.rename(columns={'Sequence':'motif_align'})
		tab_merged = tab_merged.rename(columns={'Primary_Acc_x':'UniProt'}) # change the name of the column containing the UniProt Ids
		tab_merged = tab_merged[["Accession","ELMIdentifier","UniProt","PDB", "Start", "End","motif_seq","structure","motif_align","PNG_file","PDB_motif"]]
		structure_aligned = []
		
		#### For each motif, add the gaps for the structure ####
		if tab_merged.shape[0] > 1 :
			for index, rows in tab_merged.iterrows():
				if len(rows["structure"]) == len(rows["motif_align"]) :
					structure_aligned.append(rows["structure"])
				else :
					struct = ""
					j = 0
					for i in rows["motif_align"] :
						if i == "-" :
							struct = struct + "-"
						else :
							struct = struct + rows["structure"][j]
							j += 1
					structure_aligned.append(struct)
		else :
			structure_aligned.append(rows["structure"])
			tab_merged["motif_align"] = tab_merged["motif_seq"]
		tab_merged["structure_aligned"] = structure_aligned
		tab_merged = tab_merged[["Accession","ELMIdentifier","UniProt","PDB", "Start", "End","motif_seq","structure","motif_align","structure_aligned","PNG_file","PDB_motif"]]
		
		#### Print a file for each motif type ####
		result = results_dir + "/" + elm + ".tsv"
		tab_merged.to_csv(result,header = True, sep = "\t", quoting=csv.QUOTE_NONE, index=False)	

if __name__ == "__main__":
	pdb_elm()
