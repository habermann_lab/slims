#!/usr/bin/python3
# -*- coding: utf-8 -*-

########## Convert a UniProt fasta to a tsv file ##########

#### Import modules ####
import click
import subprocess
import os
from Bio import SeqIO

def fasta2tab(fasta_file,tab_file) :
	out = open(tab_file, "w")
	print("Converting the fasta to tsv")
	print("Primary_Acc\tSequence", file = out)
	for record in SeqIO.parse(fasta_file, "fasta"):
		if len(record.id.split("|")) > 1 :
			rec = record.id.split("|")[1]
			print("%s\t%s" % (rec, record.seq),file = out)
		else :
			print("%s\t%s" % (record.id, record.seq),file = out)
	out.close()
	print("Conversion done")

if __name__ == "__main__":
	fasta2tab()
